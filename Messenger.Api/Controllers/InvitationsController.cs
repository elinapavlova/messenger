﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Api.Dtos;
using Messenger.Core.Contracts;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.Invitation;
using Messenger.Core.Mappers;
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Messenger.Api.Controllers;

[ApiVersion("1.0")]
[Authorize]
[ApiController]
[Route("/api/v{version:apiVersion}/[controller]")]
public class InvitationsController : Controller
{
    private readonly IGroupChatService _chatService;
    private readonly IUserService _userService;
    private readonly IInvitationService _invitationService;

    public InvitationsController
    (
        IGroupChatService chatService, 
        IUserService userService,
        IInvitationService invitationService
    )
    {
        _chatService = chatService;
        _userService = userService;
        _invitationService = invitationService;
    }
        
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<InvitationCreateResponse>> Create(InvitationCreateRequestDto dto)
    {
        var chat = await _chatService.GetById(dto.GroupChatId);
        if (chat is null)
        {
            return NotFound("Group chat doesn't exist");
        }

        var chatUser = chat.ChatUsers.FirstOrDefault(x => x.Id == dto.ChatUserId);
        if (chatUser is null)
        {
            return BadRequest("User is not in chat");
        }

        var result = await _invitationService.Add(new InvitationCreateRequest(new ChatUserEntity
        {
            Id = chatUser.Id, ChatId = chat.Id, UserId = chatUser.UserId, Name = chatUser.Name, DateStarted = chatUser.DateStarted
        }, GroupChatMapper.Map(chat)));
            
        if (result == null)
        {
            return BadRequest("Failed create invitation");
        }
            
        return Ok(result);
    }

    [HttpPost("{id:guid}/enter")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> GoInToChatByInvitation(Guid id, ChatEntryByInvitationRequestDto dto)
    {
        var invitation = await _invitationService.GetById(id);
        if (invitation is null)
        {
            return NotFound("Invitation doesn't exist");
        }

        var user = await _userService.GetById(dto.UserId);
        if (user is null)
        {
            return NotFound("User doesn't exist");
        }

        var result = await _invitationService.EnterChatByInvitation(new ChatEntryByInvitationRequest
        {
            Invitation = InvitationMapper.Map(invitation), User = UserMapper.Map(user)
        });
        if (result is null)
        {
            return BadRequest("Failed enter chat");
        }
            
        return Ok(result);
    }
}