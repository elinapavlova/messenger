﻿using System;

namespace Messenger.Core.Dtos.GroupChatUser;

public class GroupChatUserResponse
{
    public Guid Id { get; set; }
    public Guid ChatId { get; set; }
    public Guid UserId { get; set; }
    public string Name { get; set; }
    public DateTime DateStarted { get; set; }
}