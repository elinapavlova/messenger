﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Core.Contracts;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.ChatUser;
using Messenger.Core.Dtos.Invitation;
using Messenger.Core.Mappers;
using Messenger.Core.Options;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Messenger.Core.Logger;

namespace Messenger.Core.Services;

public class InvitationService : IInvitationService
{
    private readonly IGroupChatService _chatService;
    private readonly IChatUserService _chatUserService;
    private readonly IInvitationRepository _invitationRepository;
    private readonly Uri _baseUrl;
    private readonly int _maxCountOfActualInvitations;
    private readonly ILogger _logger;

    public InvitationService
    (
        IGroupChatService chatService, 
        IInvitationRepository invitationRepository, 
        IChatUserService chatUserService,
        AppOptions appOptions,
        ILogger logger
    )
    {
        _chatService = chatService;
        _invitationRepository = invitationRepository;
        _chatUserService = chatUserService;
        _baseUrl = new Uri(appOptions.BaseUrl);
        _maxCountOfActualInvitations = appOptions.MaxCountOfActualInvitations;
        _logger = logger;
    }
        
    public async Task<InvitationCreateResponse?> Add(InvitationCreateRequest? request)
    {
        if (await IsAddRequestValid(request) is false)
        {
            return null;
        }

        var chatUser = await _chatUserService.GetByChatIdUserId(request.ChatUser.ChatId, request.ChatUser.UserId);
        if (chatUser is null)
        {
            return null;
        }
        var invitation = await _invitationRepository.Add(new InvitationEntity
        {
            ChatUserId = chatUser.Id, DateExpired = request.DateExpired, EntriesCountMax = request.EntriesCountMax,
            DateCreated = DateTime.UtcNow, ChatId = request.Chat.Id
        });

        return invitation is null 
            ? null 
            : InvitationMapper.MapResponseCreated(_baseUrl, invitation.Id);
    }

    public async Task<InvitationResponse?> GetById(Guid id)
    {
        var invitation = await _invitationRepository.GetById(id);
        return invitation is null 
            ? null 
            : InvitationMapper.MapResponse(invitation);
    }

    public async Task<ChatUserResponse?> EnterChatByInvitation(ChatEntryByInvitationRequest? request)
    {
        if (IsEnterChatByInvitationIsValid(request) is false)
        {
            return null;
        }
        var chatUser = await GetChatUser(request.User.Id, request.Invitation.ChatId);
        if (chatUser is not null)
        {
            return chatUser;
        }
            
        var invitation = await _invitationRepository.GetById(request.Invitation.Id);
        if (invitation is null)
        {
            return null;
        }
            
        await using var transaction = await _invitationRepository.BeginTransaction();
        try
        {
            if (transaction is null)
            {
                throw new Exception("Failed start transaction");
            }
                
            chatUser = await CreateChatUser(invitation.ChatId, request.User.Id);
            await Update(invitation);
            return chatUser;
        }
        catch(Exception exception)
        {
            _logger.Error(exception.Message);
            if (transaction is not null)
            {
                transaction.HasError = true;
            }
            return null;
        }
    }

    private async Task<bool> IsAddRequestValid(InvitationCreateRequest? request)
    {
        if (request?.Chat is null || request.ChatUser is null || request.DateExpired <= DateTime.UtcNow || 
            request.EntriesCountMax <= 0)
        {
            return false;
        }

        var isChatExist = await _chatService.IsGroupChatExist(request.Chat.Id);
        var actualInvitationsCount = await _invitationRepository.CountActualInvitationsForChat(request.Chat.Id);
        return isChatExist && actualInvitationsCount < _maxCountOfActualInvitations;
    }

    private static bool IsEnterChatByInvitationIsValid(ChatEntryByInvitationRequest? request)
    {
        return request?.Invitation is not null && request.User is not null;
    }

    private async Task<ChatUserResponse?> GetChatUser(Guid userId, Guid chatId)
    {
        var chatUser = await _chatUserService.GetByChatIdUserId(chatId, userId);
        return chatUser ?? null;
    }

    private async Task<ChatUserResponse> CreateChatUser(Guid chatId, Guid userId)
    {
        var result = await _chatUserService.Add(new ChatUserEntity
        {
            ChatId = chatId, UserId = userId
        });
        if (result is null)
        {
            throw new Exception($"Failed create chat user by user Id {userId} in chat {chatId}");
        }

        return result;
    }
        
    private async Task Update(InvitationEntity invitation)
    {
        if (invitation.EntriesCount > invitation.EntriesCountMax || invitation.DateExpired >= DateTime.UtcNow)
        {
            throw new Exception($"Failed update invitation {invitation.Id}");
        }

        invitation.EntriesCount += 1;
        var result = await _invitationRepository.Update(invitation);
        if (result is null)
        {
            throw new Exception("Failed update invitation");
        }
    }
}