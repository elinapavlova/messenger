﻿using System;
using System.Collections.Generic;

namespace Messenger.Database.Entities;

public class UserEntity : BaseEntity<Guid>
{
    public string UserName { get; set; }
    public string Phone { get; set; }
    public DateTime DateCreated { get; set; }
        
    public List<ChatEntity> Chats { get; set; }
    public List<ContactEntity> Contacts { get; set; }
    public List<ChatUserEntity> ChatUsers { get; set; }
        
    public List<RefreshTokenEntity> RefreshTokens { get; set; }
}