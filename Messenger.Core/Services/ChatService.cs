﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.Chat;
using Messenger.Core.Logger;
using Messenger.Core.Mappers;
using Messenger.Core.Options;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;

namespace Messenger.Core.Services;

public class ChatService : IChatService
{
    private readonly IChatRepository _chatRepository;
    private readonly IChatUserRepository _chatUserRepository;
    private readonly IMessageFileService _messageService;
    private readonly IUserRepository _userRepository;
    private readonly IChatUserContactNameService _chatUserContactNameService;
    private readonly PagingOptions _pagingOptions;
    private readonly ILogger _logger;

    public ChatService
    (
        IChatRepository chatRepository,
        IChatUserRepository chatUserRepository,
        IMessageFileService messageService,
        IUserRepository userRepository,
        IChatUserContactNameService chatUserContactNameService,
        FilterOptions filterOptions,
        ILogger logger
    )
    {
        _chatRepository = chatRepository;
        _chatUserRepository = chatUserRepository;
        _messageService = messageService;
        _userRepository = userRepository;
        _chatUserContactNameService = chatUserContactNameService;
        _pagingOptions = filterOptions.Filters["Chats"] ?? filterOptions.Filters["Default"];
        _logger = logger;
    }

    public async Task<ChatCreateResponse?> Add(ChatCreateRequest? request)
    {
        if (await IsAddRequestValid(request) is false)
        {
            return null;
        }
        await using var transaction = await _chatRepository.BeginTransaction();
        try
        {
            if (transaction is null)
            {
                throw new Exception("Failed start transaction");
            }

            var chat = await CreateChat(request.UserCreate, request.UserWith);
            var chatUsers = await GetChatUsers(chat.Id, request.UserCreate, request.UserWith);
            chatUsers = await CreateChatUsers(chatUsers);
            chat.ChatUsers = chatUsers;
                
            return ChatMapper.MapCreatedResponse(chat);
        }
        catch(Exception exception)
        {
            _logger.Error(exception.Message);
            if (transaction is not null)
            {
                transaction.HasError = true;
            }
            return null;
        }
    }

    public async Task<ChatResponse?> GetById(Guid id)
    {
        var chat = await _chatRepository.GetById(id, false);
        if (chat is null)
        {
            return null;
        }
        chat.ChatUsers = await GetChatUsersByChatId(id);
        return ChatMapper.MapResponse(chat);
    }
        
    public async Task<ChatMessagesResponse?> GetByIdWithMessages(Guid id, FilterDto filter)
    {
        var chat = await _chatRepository.GetById(id, false);
        if (chat is null)
        {
            return null;
        }
        chat.ChatUsers = await GetChatUsersByChatId(id);
        var messages = await _messageService.GetMessagesByChatIdWithFiles(chat.Id, filter);
        return ChatMapper.MapChatMessagesResponse(chat, messages);
    }

    public async Task<ChatResponse?> Remove(Guid id)
    {
        var chat = await _chatRepository.Remove(id);
        return chat is null 
            ? null 
            : ChatMapper.MapResponse(chat);
    }

    public async Task<List<ChatResponse>?> GetByUserId(Guid userId, FilterDto filter)
    {
        var chats = await _chatRepository.GetByUserId(userId, FilterMapper.Map(filter, _pagingOptions));
        if (chats is null)
        {
            return null;
        }
        
        foreach (var chat in chats)
        {
            chat.ChatUsers = await GetChatUsersByChatId(chat.Id);
        }
        return ChatMapper.MapResponse(chats);
    }

    private async Task<bool> IsAddRequestValid(ChatCreateRequest? request)
    {
        if (request is null || request.UserCreate.Id == request.UserWith.Id)
        {
            return false;
        }

        var users = await GetUsersByIds(request.UserCreate.Id, request.UserWith.Id);
        if (users is null)
        {
            return false;
        }

        var isChatExist = await IsChatExist(request);
        return isChatExist is false;
    }

    private async Task<List<UserEntity>?> GetUsersByIds(Guid userCreateId, Guid userWithId)
    {
        var users = new List<UserEntity>();
        var userCreate = await GetUserById(userCreateId);
        if (userCreate is null)
        {
            return null;
        }
        users.Add(userCreate);
        var userWith = await GetUserById(userWithId);
        if (userWith is null)
        {
            return null;
        }
        users.Add(userWith);
        return users;
    }

    private async Task<UserEntity?> GetUserById(Guid userId)
    { 
        return await _userRepository.GetById(userId);
    }

    private async Task<bool> IsChatExist(ChatCreateRequest request)
    {
        return await _chatRepository.IsChatExist(request.UserCreate.Id, request.UserWith.Id);
    }

    private async Task<ChatEntity> CreateChat(UserEntity userCreate,UserEntity userWith)
    {
        var chat = await _chatRepository.Add(userCreate, userWith);
        if (chat is not null)
        {
            return chat;
        }
        throw new Exception($"Failed create chat for users {userCreate.Id} {userWith.Id}");
    }

    private async Task<List<ChatUserEntity>?> CreateChatUsers(List<ChatUserEntity> chatUsers)
    {
        var result = await _chatUserRepository.AddList(chatUsers);
        if (result is not null)
        {
            return result;
        }
        throw new Exception(CreateErrorText(chatUsers));
    }

    private async Task<List<ChatUserEntity>> GetChatUsers(Guid chatId, UserEntity userCreate, UserEntity userWith)
    {
        var items = new List<ChatUserEntity>();
        var chatUser = await GetChatUser(chatId, userCreate.Id, userWith.Id);
        if (chatUser is null)
        {
            throw new Exception($"Not found user in chat ChatId {chatId} UserId {userCreate.Id}");
        }
        items.Add(chatUser);
            
        chatUser = await GetChatUser(chatId, userWith.Id, userCreate.Id);
        if (chatUser is null)
        {
            throw new Exception($"Not found user in chat ChatId {chatId} UserId {userWith.Id}");
        }
        items.Add(chatUser);
        return items;
    }

    private async Task<ChatUserEntity?> GetChatUser(Guid chatId, Guid user, Guid contactUser)
    {
        return await _chatUserContactNameService.GetChatUserWithContactName(chatId, user, contactUser);
    }

    private async Task<List<ChatUserEntity>?> GetChatUsersByChatId(Guid chatId)
    { 
        return await _chatUserRepository.GetByChatId(chatId);
    }

    private static string CreateErrorText(IEnumerable<ChatUserEntity>? chatUsers)
    {
        var text = chatUsers?.Aggregate("Failed add users", (current, chatUser) => current + $" {chatUser.UserId}");
        text += " in a new chat";
        return text;
    }
}