﻿using Messenger.Core.Contracts.Provider;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Messenger.Core.Providers;

public static class DependencyProviderExtensions
{
    public static void Register<T>(this IServiceCollection services)
        where T : IDependencyProvider, new()
    {
        new T().Register(services);
    }

    public static void Register<T>(this IServiceCollection services, IConfiguration configuration)
        where T : IDependencyProviderWithConfiguration, new()
    {
        new T().Register(services, configuration);
    }
}