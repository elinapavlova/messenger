﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.User;

public class UserUpdateRequest
{
    public readonly UserEntity User;

    public UserUpdateRequest(UserEntity user)
    {
        User = user;
    }
}