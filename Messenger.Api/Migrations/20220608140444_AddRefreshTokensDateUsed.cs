﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Messenger.Api.Migrations
{
    public partial class AddRefreshTokensDateUsed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateUsed",
                table: "RefreshTokens",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateUsed",
                table: "RefreshTokens");
        }
    }
}
