﻿using System;
using Messenger.Common;

namespace Messenger.Core.Dtos.User;

public class UserLoginResponse
{
    public Guid Id { get; set; }
    public string UserName { get; set; }
    public string Phone { get; set; }
    public AccessToken AccessToken { get; set; }
}