﻿#nullable enable
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Http;

namespace Messenger.Core.Dtos.File;

public class FilesUploadRequest
{
    public readonly ChatUserEntity ChatUser;
    public readonly IFormFileCollection Files;
    public readonly string? Text;
    public readonly MessageEntity? ReferenceMessage;


    public FilesUploadRequest(ChatUserEntity chatUser, IFormFileCollection files, string? text, MessageEntity? referenceMessage)
    {
        ChatUser = chatUser;
        Files = files;
        Text = text;
        ReferenceMessage = referenceMessage;
    }
}