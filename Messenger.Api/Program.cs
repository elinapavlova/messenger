using System;
using System.Threading.Tasks;
using Messenger.Database;
using Messenger.Database.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Messenger.Api;

public static class Program
{
    public static async Task Main(string[] args)
    {
        try
        {
            var host = CreateHostBuilder(args).Build();
            using (var container = host.Services.CreateScope())
            {
                Console.WriteLine("Start migrations");
                var context = container.ServiceProvider.GetRequiredService<AppDbContext>();
                await context.Database.MigrateAsync();
            }
            
            Console.WriteLine("Start work");
            await host.RunAsync();
        }
        catch (Exception e)
        {
            await Console.Error.WriteLineAsync("Stopped program because of exception\r\n" +
                                               $"Data: {e.Data}\r\n" +
                                               $"Message: {e.Message}\r\n" +
                                               $"Trace: \r\n {e.StackTrace}"
            );
        }

    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((_, config) =>
            {
                config.AddJsonFile("appsettings.Development.json")
                    .AddJsonFile("appsettings.json", true);
            })
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            })
            .ConfigureLogging(logging =>
            {
                logging.ClearProviders();
                logging.SetMinimumLevel(LogLevel.Trace);
            });
}