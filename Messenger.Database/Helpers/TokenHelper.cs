﻿using System;
using Messenger.Database.Entities;

namespace Messenger.Database.Helpers;

public static class TokenHelper
{
    public static bool IsExpired(long expiration)
    {
        return DateTime.UtcNow.Ticks >= expiration;
    }
        
    public static bool IsTokenValid(string token)
    {
        return string.IsNullOrEmpty(token) is false &&
               string.IsNullOrWhiteSpace(token) is false;
    }
        
    public static bool RefreshTokenFilter(DateTime tokenDateExpired, DateTime? tokenDateUsed)
    {
        return tokenDateExpired >= DateTime.UtcNow &&
               tokenDateUsed == null;
    }
}