﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Dtos.Message;

namespace Messenger.Core.Contracts.Services;

public interface IMessageFileService
{
    Task<MessageResponse?> RemoveMessageByIdWithFiles(Guid id);
    Task<MessageResponseWithFiles?> GetMessageByIdWithFiles(Guid id);
    Task<List<MessageResponseWithFiles>?> GetMessagesByChatIdWithFiles(Guid chatId, FilterDto filter);
    Task<MessageResponse?> Add(MessageCreateRequest request);
}