﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Database.Entities;

namespace Messenger.Database.Contracts;

public interface IChatUserRepository
{
    Task<ChatUserEntity?> Add(ChatUserEntity chatUser);
    Task<List<ChatUserEntity>?> AddList(List<ChatUserEntity> chatUsers);
    Task<ChatUserEntity?> Update(ChatUserEntity updatedChatUser);
    Task<ChatUserEntity?> GetById(Guid id);
    Task<List<ChatUserEntity>?> GetByChatId(Guid chatId);
    Task<ChatUserEntity?> GetByChatIdUserId(Guid chatId, Guid userId);
}