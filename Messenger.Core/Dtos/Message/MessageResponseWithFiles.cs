﻿#nullable enable
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Messenger.Core.Dtos.Message;

public class MessageResponseWithFiles
{
    public Guid Id { get; set; }
    public string? Text { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime? DateDeleted { get; set; }
    public DateTime? DateUpdated { get; set; }
    public Guid ChatUserId { get; set; }
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public ReferenceMessage? ReferenceMessage { get; set; }
    public List<Uri>? Files { get; set; }
}