﻿using System;
using Messenger.Database.Entities;

namespace Messenger.Database.Entities;

public class ContactEntity : BaseEntity<Guid>
{
    public Guid UserId { get; set; }
    public Guid ContactUserId { get; set; }
    public string ContactName { get; set; }
    public DateTime DateCreated { get; set; }
        
    public UserEntity User { get; set; }
}