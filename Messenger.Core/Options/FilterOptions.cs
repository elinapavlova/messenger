﻿using System.Collections.Generic;

namespace Messenger.Core.Options;

public class FilterOptions
{
    public Dictionary<string, PagingOptions> Filters { get; set; }
}

public class PagingOptions
{
    public int DefaultPageSize { get; set; }
    public int DefaultPageNumber { get; set; }
}