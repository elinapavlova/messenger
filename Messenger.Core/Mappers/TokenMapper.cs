﻿using Messenger.Common;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class TokenMapper
{
    public static AccessToken Map(string accessToken, string refreshToken, long expiration)
    {
        return new AccessToken
        {
            Token = accessToken,
            RefreshToken = refreshToken,
            Expiration = expiration
        };
    }
}