﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.GroupChat;
using Messenger.Core.Dtos.GroupChatUser;

namespace Messenger.Core.Contracts.Services;

public interface IGroupChatService
{
    Task<GroupChatCreateResponse?> Add(GroupChatCreateRequest request);
    Task<GroupChatResponse?> GetById(Guid id);
    Task<GroupChatResponse?> Update(GroupChatUpdateRequest request);
    Task<GroupChatResponse?> Remove(Guid id);
    Task<bool> IsGroupChatExist(Guid id);
}