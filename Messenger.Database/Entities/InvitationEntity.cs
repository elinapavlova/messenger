﻿using System;

namespace Messenger.Database.Entities;

public class InvitationEntity : BaseEntity<Guid>
{
    public DateTime DateCreated { get; set; }
    public DateTime? DateExpired { get; set; }
    public int? EntriesCountMax { get; set; }
    public int EntriesCount { get; set; }
    public Guid ChatUserId { get; set; }
    public Guid ChatId { get; set; }
        
    public ChatUserEntity ChatUser { get; set; }
    public ChatEntity Chat { get; set; }
}