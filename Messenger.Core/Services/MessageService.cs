﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.Message;
using Messenger.Core.Mappers;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;

namespace Messenger.Core.Services;

public class MessageService : IMessageService
{
    private readonly IMessageRepository _messageRepository;
    private readonly IChatUserService _chatUserService;

    public MessageService
    (
        IMessageRepository messageRepository,
        IChatUserService chatUserService
    )
    {
        _messageRepository = messageRepository;
        _chatUserService = chatUserService;
    }
    
    
    public async Task<MessageEntity> Add(Guid chatUserId, string? messageText, Guid? referenceMessageId = null)
    {
        var chatUser = await GetChatUserById(chatUserId);
        var message = await CreateMessage(chatUser.Id, chatUser.ChatId, messageText, referenceMessageId);
        message.ChatUser = chatUser;
        return message;
    }

    public async Task<MessageResponse?> GetById(Guid id)
    {
        var message = await _messageRepository.GetById(id);
        return message is null 
            ? null
            : MessageMapper.MapResponse(message);
    }
    
    private async Task<MessageEntity> CreateMessage(Guid chatUserId, Guid chatId, string? text, Guid? referenceMessageId)
    {
        var message = await _messageRepository.Add(new MessageEntity
        {
            ChatUserId = chatUserId, 
            ChatId = chatId, 
            Text = text,
            ReferenceMessageId = referenceMessageId
        }, true);
        if (message is null)
        {
            throw new Exception("Failed create message while uploading file. " +
                                $"Chat Id {chatId} ChatUser Id {chatUserId} Text {text}");
        }
        return message;
    }
    
    private async Task<ChatUserEntity> GetChatUserById(Guid chatUserId)
    {
        var chatUser = await _chatUserService.GetById(chatUserId);
        if (chatUser is null)
        {
            throw new Exception($"Failed upload files. ChatUser by Id {chatUserId} doesn't exist");
        }
        return ChatUserMapper.Map(chatUser);
    }

    public async Task<MessageResponse?> Update(MessageUpdateRequest dto)
    {
        var message = await _messageRepository.Update(dto.Message);
        return message is null 
            ? null
            : MessageMapper.MapResponse(message);
    }

    public async Task<LastMessageInChat?> GetLastByChatId(Guid chatId)
    {
        var lastMessage = await _messageRepository.GetLastByChatId(chatId);
        return lastMessage is null
            ? null
            : MessageMapper.MapLastMessageInChat(lastMessage);
    }
}