﻿namespace Messenger.Api.Dtos;

public record UserCreateRequestDto
{
    public string UserName { get; set; }
    public string Phone { get; set; }
}