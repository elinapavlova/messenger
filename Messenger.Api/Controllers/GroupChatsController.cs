﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Api.Dtos;
using Messenger.Common.Filter;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.File;
using Messenger.Core.Dtos.GroupChat;
using Messenger.Core.Dtos.GroupChatUser;
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Messenger.Api.Controllers;

[ApiVersion("1.0")]
[Authorize]
[ApiController]
[Route("/api/v{version:apiVersion}/[controller]")]
public class GroupChatsController : Controller
{
    private readonly IGroupChatService _chatService;
    private readonly IUserService _userService;
    private readonly IFileService _fileService;
    private readonly IChatUserService _chatUserService;
    private readonly IGroupChatMessageService _groupChatMessageService;

    public GroupChatsController
    (
        IGroupChatService chatService, 
        IUserService userService,
        IFileService fileService,
        IChatUserService chatUserService,
        IGroupChatMessageService groupChatMessageService
    )
    {
        _chatService = chatService;
        _userService = userService;
        _fileService = fileService;
        _chatUserService = chatUserService;
        _groupChatMessageService = groupChatMessageService;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<GroupChatCreateResponse>> Create(GroupChatCreateRequestDto dto)
    {
        var users = new List<UserEntity>();
        if (dto?.UserIds is null)
        {
            return BadRequest("No users to create group chat");
        }
            
        var creator = await _userService.GetById(dto.CreatorId);
        if (creator is null)
        {
            return NotFound("Creator doesn't exist");
        }

        var notFoundUserIds = new List<Guid>();
        foreach (var id in dto.UserIds)
        {
            var user = await _userService.GetById(id);
            if (user is null)
            {
                notFoundUserIds.Add(id);
                continue;
            }
            users.Add(new UserEntity
            {
                Id = user.Id, Phone = user.Phone, UserName = user.UserName
            });
        }
        if (notFoundUserIds.Any())
        {
            var errorMessage = notFoundUserIds.Aggregate("Some users don't exist:", (current, id) => current + $"\r\n{id}");
            return NotFound(errorMessage);
        }

        var result = await _chatService.Add(new GroupChatCreateRequest(new ChatEntity
        {
            IsGroup = true, Name = dto.Name, CreatorId = dto.CreatorId
        }, users));
        if (result is null)
        {
            return BadRequest("Failed create group chat");
        }

        return Ok(result);
    }
        
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<GroupChatResponse>> Update(GroupChatUpdateRequestDto dto)
    {
        var groupChat = await _chatService.GetById(dto.Id);
        if (groupChat is null)
        {
            return NotFound("Group chat doesn't exist");
        }

        var result = await _chatService.Update(new GroupChatUpdateRequest(new ChatEntity
        {
            Id = dto.Id, Name = dto.NewName
        }));
        if (result is null)
        {
            return BadRequest("Chat name is not valid");
        }

        return Ok(result);
    }
        
    [HttpGet("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<GroupChatResponse>> GetById(Guid id)
    {
        var groupChat = await _chatService.GetById(id);
        if (groupChat is null)
        {
            return NotFound("Group chat doesn't exist");
        }

        return Ok(groupChat);
    }
        
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Remove(Guid id)
    {
        var groupChat = await _chatService.Remove(id);
        if (groupChat is null)
        {
            return NotFound("Group chat doesn't exist");
        }

        return NoContent();
    }

    [HttpPost("{id:guid}/users")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<GroupChatResponse>> AddUser(Guid id, Guid userId)
    {
        var groupChat = await _chatService.GetById(id);
        if (groupChat is null)
        {
            return NotFound("Group chat doesn't exist");
        }

        var user = await _userService.GetById(userId);
        if (user is null)
        {
            return NotFound("User doesn't exist");
        }
            
        var result = await _chatUserService.AddUserToGroupChat(new GroupChatUserCreateRequest
        {
            GroupChat = new ChatEntity{Id = groupChat.Id, CreatorId = groupChat.CreatorId}, 
            User = new UserEntity{Id = user.Id}
        });
        if (result is null)
        {
            return BadRequest("User is already in chat");
        }

        return Ok(result);
    }

    [HttpGet("{id:guid}/messages")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<GroupChatMessagesResponse>> GetByIdWithMessages(Guid id, [FromQuery] FilterDto filter)
    {
        var groupChat = await _chatService.GetById(id);
        if (groupChat is null)
        {
            return NotFound("Group chat doesn't exist");
        }
        var result = await _groupChatMessageService.GetGroupChatByIdWithMessages(id, filter);
        return Ok(result);
    }
        
    [HttpGet("{id:guid}/files")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<FileResponse>> GetFilesByGroupChatId(Guid id, [FromQuery] FilterDto filter)
    {
        var chat = await _chatService.GetById(id);
        if (chat is null)
        {
            return NotFound("Group chat doesn't exist");
        }

        var result = await _fileService.GetByChatId(id, filter);
            
        return Ok(result);
    }
}