﻿using System;

namespace Messenger.Core.Dtos.Invitation;

public class InvitationResponse
{
    public Guid Id { get; set; }
    public Guid ChatUserId { get; set; }
    public Guid ChatId { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime? DateExpired { get; set; }
    public int? EntriesCountMax { get; set; }
    public int EntriesCount { get; set; }
}