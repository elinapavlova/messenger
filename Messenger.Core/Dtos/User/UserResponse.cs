﻿using System;

namespace Messenger.Core.Dtos.User;

public class UserResponse
{
    public Guid Id { get; set; }
    public string UserName { get; set; }
    public string Phone { get; set; }
}