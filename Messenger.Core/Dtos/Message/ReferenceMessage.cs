﻿#nullable enable
using System;
using System.Collections.Generic;

namespace Messenger.Core.Dtos.Message;

public class ReferenceMessage
{
    public Guid Id { get; set; }
    public string? Text { get; set; }
    public DateTime? DateDeleted { get; set; }
    public DateTime DateCreated { get; set; }
    public Guid ChatUserId { get; set; }
    public Guid ChatId { get; set; }
    public List<Uri>? Files { get; set; }
}