﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace Messenger.Database;

public class TransactionContainer : IAsyncDisposable
{
    public bool HasError { get; set; }
    public IDbContextTransaction ContextTransaction { get; set; }
    public async ValueTask DisposeAsync()
    {
        if (HasError is false)
        {
            await ContextTransaction.CommitAsync();
            return;
        }
        await ContextTransaction.RollbackAsync();
    }
}