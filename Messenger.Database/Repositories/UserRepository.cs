﻿#nullable enable
using System;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Helpers;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Messenger.Database.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Repositories;

public class UserRepository : BaseRepository<UserEntity, Guid>, IUserRepository
{
    public UserRepository(AppDbContext context) : base(context)
    {
    }

    public async Task<UserEntity?> Add(UserEntity user)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            user.UserName = user.UserName?.Trim();
            if (UserHelper.IsUserNameValid(user.UserName) is false || UserHelper.IsPhoneValid(user.Phone) is false)
            {
                return null;
            }
            user.DateCreated = DateTime.UtcNow;
            var entry = await dbSet.AddAsync(user);
            return entry.Entity;                
        });
    }
        
    public async Task<UserEntity?> Update(UserEntity userUpdated)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            userUpdated.UserName = userUpdated.UserName?.Trim();
            var user = await GetById(userUpdated.Id);
            if (UserHelper.IsUserNameValid(userUpdated.UserName) is false || user == null)
            {
                return null;
            }
            user.UserName = userUpdated.UserName;
            var entry = dbSet.Update(user);
            return entry.Entity;
        });
    }

    public async Task<UserEntity?> GetById(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.FirstOrDefaultAsync(u => u.Id == id);
        });
    }

    public async Task<UserEntity?> GetByPhone(string? phone)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            phone = phone?.Trim();
            if (UserHelper.IsPhoneValid(phone) is false)
            {
                return null;
            }
            return await dbSet.FirstOrDefaultAsync(u => u.Phone == phone);
        });
    }

    public async Task<UserEntity?> Remove(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var user = await dbSet.FirstOrDefaultAsync(u => u.Id == id);
            if (user is null)
            {
                return null;
            }

            var entry = dbSet.Remove(user);
            return entry.Entity;
        });
    }
}