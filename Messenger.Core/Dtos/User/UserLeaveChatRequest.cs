﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.User;

public class UserLeaveChatRequest
{
    public readonly UserEntity User;
    public readonly ChatEntity Chat;

    public UserLeaveChatRequest(UserEntity user, ChatEntity chat)
    {
        User = user;
        Chat = chat;
    }
}