﻿using System;

namespace Messenger.Api.Dtos;

public record ChatCreateRequestDto
{
    public Guid UserCreateId { get; set; }
    public Guid UserWithId { get; set; }
}