﻿using System;
using System.Threading.Tasks;
using Messenger.Api.Dtos;
using Messenger.Common.Filter;
using Messenger.Core.Contracts;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.Chat;
using Messenger.Core.Dtos.File;
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Messenger.Api.Controllers;

[ApiVersion("1.0")]
[ApiController]
[Authorize]
[Route("/api/v{version:apiVersion}/[controller]")]
public class ChatsController : Controller
{
    private readonly IChatService _chatService;
    private readonly IUserService _userService;
    private readonly IFileService _fileService;

    public ChatsController
    (
        IChatService chatService, 
        IUserService userService,
        IFileService fileService
    )
    {
        _chatService = chatService;
        _userService = userService;
        _fileService = fileService;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<ChatCreateResponse>> Create(ChatCreateRequestDto dto)
    {
        var userCreate = await _userService.GetById(dto.UserCreateId);
        var userWith = await _userService.GetById(dto.UserWithId);
        if (userCreate is null || userWith is null)
        {
            return NotFound("Some users don't exist");
        }

        var result = await _chatService.Add(new ChatCreateRequest(new UserEntity 
            {
                Id = userCreate.Id, Phone = userCreate.Phone, UserName = userCreate.UserName
            }, 
            new UserEntity
            {
                Id = userWith.Id, Phone = userWith.Phone, UserName = userWith.UserName
            }));
        if (result is null)
        {
            return BadRequest("Failed chat create");
        }

        return Ok(result);
    }

    [HttpGet("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<ChatResponse>> GetById(Guid id)
    {
        var chat = await _chatService.GetById(id);
        if (chat is null)
        {
            return NotFound("Chat doesn't exist");
        }

        return Ok(chat);
    }
        
    [HttpGet("{id:guid}/messages")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<ChatMessagesResponse>> GetByIdWithMessages(Guid id, [FromQuery] FilterDto filter)
    {
        var chat = await _chatService.GetByIdWithMessages(id, filter);
        if (chat is null)
        {
            return NotFound("Chat doesn't exist");
        }

        return Ok(chat);
    }
        
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Remove(Guid id)
    {
        var chat = await _chatService.Remove(id);
        if (chat is null)
        {
            return NotFound("Chat doesn't exist");
        }

        return NoContent();
    }
        
    [HttpGet("{id:guid}/files")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<FileResponse>> GetFilesByChatId(Guid id, [FromQuery] FilterDto filter)
    {
        var chat = await _chatService.GetById(id);
        if (chat is null)
        {
            return NotFound("Chat doesn't exist");
        }

        var result = await _fileService.GetByChatId(id, filter);
            
        return Ok(result);
    }
}