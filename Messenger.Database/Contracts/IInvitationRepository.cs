﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Database.Entities;

namespace Messenger.Database.Contracts;

public interface IInvitationRepository : IBaseRepository<InvitationEntity, Guid>
{
    Task<InvitationEntity?> Add(InvitationEntity invitation);
    Task<InvitationEntity?> GetById(Guid id);
    Task<InvitationEntity?> Update(InvitationEntity updatedInvitation);
    Task<int> CountActualInvitationsForChat(Guid chatId);
}