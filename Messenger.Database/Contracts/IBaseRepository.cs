﻿#nullable enable
using System.Threading.Tasks;

namespace Messenger.Database.Contracts;

public interface IBaseRepository<TModel, TKey>
    where TModel : BaseEntity<TKey>
{
    Task<TransactionContainer?> BeginTransaction();
}