﻿using System;
using System.Threading.Tasks;
using Messenger.Api.Dtos;
using Messenger.Common.Filter;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.User;
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Messenger.Api.Controllers;

[ApiVersion("1.0")]
[Authorize]
[ApiController]
[EnableCors("CustomCorsPolicy")]
[Route("/api/v{version:apiVersion}/[controller]")]
public class UsersController : Controller
{
    private readonly IUserService _userService;
    private readonly IUserChatService _userChatService;
    private readonly IContactService _contactService;
    private readonly IGroupChatService _groupChatService;

    public UsersController
    (
        IUserService userService,
        IUserChatService userChatService,
        IContactService contactService,
        IGroupChatService groupChatService
    )
    {
        _userService = userService;
        _userChatService = userChatService;
        _contactService = contactService;
        _groupChatService = groupChatService;
    }

    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<UserResponse>> Update(UserUpdateRequestDto dto)
    {
        var user = await _userService.GetById(dto.Id);
        if (user is null)
        {
            return NotFound("User doesn't exist");
        }
        var result = await _userService.Update(new UserUpdateRequest(new UserEntity
        {
            Id = dto.Id, UserName = dto.NewUserName
        }));
        if (result is null)
        {
            return BadRequest("New UserName is not valid");
        }

        return Ok(result);
    }
        
    [HttpGet("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<UserResponse>> GetById(Guid id)
    {
        var user = await _userService.GetById(id);
        if (user is null)
        {
            return NotFound("User doesn't exist");
        }

        return Ok(user);
    }
        
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<UserResponse>> GetByPhone([FromQuery] string phone)
    {
        var user = await _userService.GetByPhone(phone);
        if (user is null)
        {
            return NotFound("User doesn't exist");
        }

        return Ok(user);
    }
        
    [HttpGet("{id:guid}/chats")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetChats(Guid id, [FromQuery] FilterDto filter)
    {
        var user = await _userService.GetById(id);
        if (user is null)
        {
            return NotFound("User doesn't exist");
        }

        var result = await _userChatService.GetChats(id, filter);
        return Ok(result);
    }

    [HttpGet("{id:guid}/contacts")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetContactsByUserId(Guid id, [FromQuery] FilterDto filter)
    {
        var user = await _userService.GetById(id);
        if (user is null)
        {
            return NotFound("User doesn't exist");
        }
        var result = await _contactService.GetByUserId(id, filter);
        return Ok(result);
    }
        
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Remove(Guid id)
    {
        var message = await _userService.Remove(id);
        if (message is null)
        {
            return NotFound("User doesn't exist");
        }
            
        return NoContent();
    }
        
    [HttpPut("{id:guid}/chats/{chatId:guid}/leave")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<UserResponse>> LeaveChat(Guid id, Guid chatId)
    {
        var user = await _userService.GetById(id);
        if (user is null)
        {
            return NotFound("User doesn't exist");
        }
            
        var chat = await _groupChatService.GetById(chatId);
        if (chat is null)
        {
            return NotFound("Chat doesn't exist");
        }

        var result = await _userChatService.LeaveChat(new UserLeaveChatRequest(new UserEntity
            {
                Id = user.Id, UserName = user.UserName, Phone = user.Phone
            }, 
            new ChatEntity
            {
                Id = chat.Id, Name = chat.Name, IsGroup = true
            }));
            
        if (result is null)
        {
            return BadRequest("User isn't in chat or chat isn't group");
        }

        return NoContent();
    }
}