﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Dtos.File;

namespace Messenger.Core.Contracts.Services;

public interface IFileService
{
    Task<FilesUploadResponse?> Upload(FilesUploadRequest request);
    Task<List<FileResponse>?> GetByMessageId(Guid messageId);
    Task<FileResponse?> Remove(Guid id);
    Uri? CreateUrl(string path);
    Task<List<FileResponse>?> GetByChatId(Guid chatId, FilterDto filter);
}