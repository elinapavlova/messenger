﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Helpers;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Messenger.Database.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Repositories;

public class ChatUserRepository : BaseRepository<ChatUserEntity, Guid>, IChatUserRepository
{
    public ChatUserRepository(AppDbContext context) : base(context)
    {
    }

    public async Task<ChatUserEntity?> Add(ChatUserEntity chatUser)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            chatUser.Name = chatUser.Name?.Trim();
            if (ChatUserHelper.IsChatUserNameValid(chatUser.Name) is false || await IsChatUserExist(chatUser))
            {
                return null;
            }

            chatUser.DateStarted = DateTime.UtcNow;
            var entry = await dbSet.AddAsync(chatUser);
            return entry.Entity;
        });
    }

    public async Task<List<ChatUserEntity>?> AddList(List<ChatUserEntity> chatUsers)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            foreach (var chatUser in chatUsers)
            {
                chatUser.Name = chatUser.Name?.Trim();
                if (ChatUserHelper.IsChatUserNameValid(chatUser.Name) is false || await IsChatUserExist(chatUser))
                {
                    return null;
                }
                chatUser.DateStarted = DateTime.UtcNow;
            }
                
            await dbSet.AddRangeAsync(chatUsers);
            return chatUsers; 
        });
    }

    public async Task<ChatUserEntity?> Update(ChatUserEntity updatedChatUser)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            updatedChatUser.Name = updatedChatUser.Name?.Trim();
            var chatUser = await dbSet.FirstOrDefaultAsync(c => c.Id == updatedChatUser.Id && c.DateComeOut == null);
            if (ChatUserHelper.IsChatUserNameValid(updatedChatUser.Name) is false || chatUser == null)
            {
                return null;
            }
            chatUser.DateComeOut = chatUser.DateComeOut;
            chatUser.Name = updatedChatUser.Name;
            var entry = dbSet.Update(chatUser);
            return entry.Entity;
        });
    }

    public async Task<ChatUserEntity?> GetById(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.FirstOrDefaultAsync(cu => cu.Id == id && cu.DateComeOut == null);
        });
    }
        
    public async Task<List<ChatUserEntity>?> GetByChatId(Guid chatId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.Where(cu => cu.ChatId == chatId && cu.DateComeOut == null).ToListAsync();
        });
    }
        
    public async Task<ChatUserEntity?> GetByChatIdUserId(Guid chatId, Guid userId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.FirstOrDefaultAsync(cu => cu.ChatId == chatId && cu.UserId == userId && cu.DateComeOut == null);
        });
    }
        
    private async Task<bool> IsChatUserExist(ChatUserEntity chatUser)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var isExist = await dbSet.AnyAsync(cu => 
                cu.ChatId == chatUser.ChatId && 
                cu.UserId == chatUser.UserId &&
                cu.DateComeOut == null);
            return isExist;
        });
    }
}