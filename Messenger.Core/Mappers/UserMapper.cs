﻿#nullable enable
using Messenger.Common;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.Chat;
using Messenger.Core.Dtos.ChatUser;
using Messenger.Core.Dtos.User;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class UserMapper
{
    public static UserCreateResponse MapCreatedResponse(UserEntity user)
    {
        return new UserCreateResponse
        {
            Id = user.Id,
            UserName = user.UserName,
            Phone = user.Phone,
            DateCreated = user.DateCreated,
        };
    }        
        
    public static UserEntity Map(UserCreateResponse user)
    {
        return new UserEntity
        {
            Id = user.Id,
            UserName = user.UserName,
            Phone = user.Phone,
            DateCreated = user.DateCreated,
        };
    }

    public static UserResponse MapResponse(UserEntity user)
    {
        return new UserResponse
        {
            Id = user.Id,
            UserName = user.UserName,
            Phone = user.Phone
        };
    }
        
    public static UserEntity Map(UserResponse user)
    {
        return new UserEntity
        {
            Id = user.Id,
            UserName = user.UserName,
            Phone = user.Phone
        };
    }

    public static UserChatResponse MapUserChatResponse(ChatResponse chat, ChatUserResponse chatUser, LastMessageInChat? lastMessage)
    {
        return new UserChatResponse
        {
            ChatId = chat.Id,
            ChatName = string.IsNullOrEmpty(chatUser.Name) 
                ? chat.Name 
                : chatUser.Name,
            DateCreated = chat.DateCreated,
            DateComeIn = chatUser.DateStarted,
            IsGroup = chat.IsGroup,
            LastMessage = lastMessage
        };
    }

    public static UserTokenInfo MapTokenInfo(UserEntity user)
    {
        return new UserTokenInfo
        {
            Id = user.Id,
            Phone = user.Phone
        };
    }    
    
    public static UserTokenInfo MapTokenInfo(UserResponse user)
    {
        return new UserTokenInfo
        {
            Id = user.Id,
            Phone = user.Phone
        };
    }
}