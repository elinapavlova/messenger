﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Common.Helpers;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Repositories;

public class ContactRepository : BaseRepository<ContactEntity, Guid>, IContactRepository
{
    public ContactRepository(AppDbContext context) : base(context)
    {
    }

    public async Task<ContactEntity?> Add(ContactEntity contact)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            contact.ContactName = contact.ContactName?.Trim();
            if (ContactHelper.IsContactNameValid(contact.ContactName) is false || await IsContactExist(contact))
            {
                return null;
            }
            contact.DateCreated = DateTime.UtcNow;
            var entry = await dbSet.AddAsync(contact);
            return entry.Entity;
        });
    }

    public async Task<ContactEntity?> Update(ContactEntity updatedContact)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            updatedContact.ContactName = updatedContact.ContactName?.Trim();
            if (ContactHelper.IsContactNameValid(updatedContact.ContactName) is false)
            {
                return null;
            }

            var contact = await dbSet.FirstOrDefaultAsync(c => c.Id == updatedContact.Id);
            if (contact == null)
            {
                return null;
            }

            contact.ContactName = updatedContact.ContactName;
            dbSet.Update(contact);
            return contact;
        });
    }

    public async Task<ContactEntity?> GetById(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.FirstOrDefaultAsync(c => c.Id == id);
        });
    }
        
    public async Task<List<ContactEntity>?> GetByUserId(Guid userId, Filter filter)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entities = dbSet.Where(c => c.UserId == userId);
            return await GetFiltered(entities, filter);
        });
    }

    public async Task<ContactEntity?> GetByUserIdContactUserId(Guid userId, Guid contactUserId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.FirstOrDefaultAsync(c => c.UserId == userId && c.ContactUserId == contactUserId);
        });
    }
        
    public async Task<ContactEntity?> Remove(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var contact = await dbSet.FirstOrDefaultAsync(c => c.Id == id);
            if (contact == null)
            {
                return null;
            }

            var entry = dbSet.Remove(contact);
            return entry.Entity;
        });
    }

    private async Task<bool> IsContactExist(ContactEntity contact)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var isExist = await dbSet.AnyAsync(c => 
                c.UserId == contact.UserId && 
                c.ContactUserId == contact.ContactUserId);
            return isExist;
        });
    }
}