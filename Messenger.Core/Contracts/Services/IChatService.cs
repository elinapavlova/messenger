﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.Chat;

namespace Messenger.Core.Contracts.Services;

public interface IChatService
{
    Task<ChatCreateResponse?> Add(ChatCreateRequest request);
    Task<ChatResponse?> GetById(Guid id);
    Task<ChatMessagesResponse?> GetByIdWithMessages(Guid id, FilterDto filter);
    Task<ChatResponse?> Remove(Guid id);
    Task<List<ChatResponse>?> GetByUserId(Guid userId, FilterDto filter);
}