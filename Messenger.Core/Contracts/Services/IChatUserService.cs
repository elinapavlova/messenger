﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Core.Dtos.ChatUser;
using Messenger.Core.Dtos.GroupChat;
using Messenger.Core.Dtos.GroupChatUser;
using Messenger.Database.Entities;

namespace Messenger.Core.Contracts.Services;

public interface IChatUserService
{
    Task<ChatUserResponse?> GetById(Guid id);
    Task<ChatUserResponse?> GetByChatIdUserId(Guid chatId, Guid userId);
    Task<ChatUserResponse?> ComeOut(Guid chatUserId);
    Task<List<ChatUserResponse>?> GetByChatId(Guid chatId);
    Task<ChatUserResponse?> Add(ChatUserEntity chatUser);
    Task<List<ChatUserEntity>?> AddList(List<ChatUserEntity> chatUsers);
    Task<GroupChatResponse?> AddUserToGroupChat(GroupChatUserCreateRequest request);
}