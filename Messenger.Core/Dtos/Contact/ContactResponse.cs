﻿using System;

namespace Messenger.Core.Dtos.Contact;

public class ContactResponse
{
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public Guid ContactUserId { get; set; }
    public string ContactName { get; set; }
}