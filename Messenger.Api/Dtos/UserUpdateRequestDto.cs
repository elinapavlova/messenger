﻿using System;

namespace Messenger.Api.Dtos;

public record UserUpdateRequestDto
{
    public Guid Id { get; set; }
    public string NewUserName { get; set; }
}