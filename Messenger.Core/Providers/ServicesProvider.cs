﻿using Messenger.Core.Contracts;
using Messenger.Core.Contracts.Provider;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Messenger.Core.Providers;

public class ServicesProvider : IDependencyProvider
{
    public void Register(IServiceCollection services)
    {
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IChatService, ChatService>();
        services.AddScoped<IGroupChatService, GroupChatService>();
        services.AddScoped<IMessageService, MessageService>();
        services.AddScoped<IContactService, ContactService>();
        services.AddScoped<IChatUserService, ChatUserService>();
        services.AddScoped<IFileService, FileService>();
        services.AddScoped<ITokenService, TokenService>();
        services.AddScoped<IAuthService, AuthService>();
        services.AddScoped<IInvitationService, InvitationService>();
        services.AddScoped<IChatUserContactNameService, ChatUserContactNameService>();
        services.AddScoped<IUserChatService, UserChatService>();
        services.AddScoped<IMessageFileService, MessageFileService>();
        services.AddScoped<IGroupChatMessageService, GroupChatMessageService>();
    }
}