﻿using System;

namespace Messenger.Api.Dtos;

public record InvitationCreateRequestDto
{
    public Guid ChatUserId { get; set; }
    public Guid GroupChatId { get; set; }
    public  DateTime? DateExpired { get; set; }
    public int? EntriesCountMax { get; set; }
}