﻿using System;

namespace Messenger.Database.Entities;

public class FileEntity : BaseEntity<Guid>
{
    public string Name { get; set; }
    public string Path { get; set; }
    public string Extension { get; set; }
    public DateTime DateCreated { get; set; }
    public Guid MessageId { get; set; }
    public Guid? ChatId { get; set; }
        
    public MessageEntity Message { get; set; } 
    public ChatEntity Chat { get; set; }
}