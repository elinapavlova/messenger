﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Common.Helpers;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Repositories;

public class ChatRepository : BaseRepository<ChatEntity, Guid>, IChatRepository
{
    public ChatRepository(AppDbContext context) : base(context)
    {
    }

    public async Task<List<ChatEntity>?> GetByUserId(Guid userId, Filter filter)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entities = dbSet
                .Where(c => c.ChatUsers.Any(cu => cu.UserId == userId) && c.DateDeleted == null)
                .OrderByDescending(c => c.DateCreated);
                
            return await GetFiltered(entities, filter);
        });
    }

    public async Task<ChatEntity?> Add(UserEntity? userCreate, UserEntity? userWith)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            if (userCreate?.Id is null || userWith?.Id is null || userCreate.Id == userWith.Id)
            {
                return null;
            }
            var chat = new ChatEntity
            {
                DateCreated = DateTime.UtcNow, CreatorId = userCreate.Id
            };
            var entry = await dbSet.AddAsync(chat);
            return entry.Entity;
        });
    }
        
    public async Task<ChatEntity?> Add(ChatEntity chat)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            chat.Name = chat.Name?.Trim();
            if (GroupChatHelper.IsGroupChatNameValid(chat.Name) is false)
            {
                return null;
            }
            chat.IsGroup = true;
            chat.DateCreated = DateTime.UtcNow;
            var entry = await dbSet.AddAsync(chat);
            return entry.Entity;
        });
    }

    public async Task<ChatEntity?> Update(ChatEntity updatedChat)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            updatedChat.Name = updatedChat.Name?.Trim();
            var chat = await dbSet.FirstOrDefaultAsync(c => c.IsGroup == true && c.Id == updatedChat.Id);
            if (GroupChatHelper.IsGroupChatNameValid(updatedChat.Name) is false || chat is not {DateDeleted: null})
            {
                return null;
            }

            chat.Name = updatedChat.Name;
            var entry = dbSet.Update(chat);
            return entry.Entity;
        });
    }
        
    public async Task<bool> IsChatExist(Guid creatorId, Guid userWithId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var isExist = await dbSet.AnyAsync(c =>
                c.IsGroup == false && c.DateDeleted == null &&
                c.ChatUsers.Any(cu => cu.UserId == creatorId) &&
                c.ChatUsers.Any(cu => cu.UserId == userWithId));
            return isExist;
        });
    }
        
    public async Task<bool> IsChatExist(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var isExist = await dbSet.AnyAsync(c => c.DateDeleted == null && c.Id == id);
            return isExist;
        });
    }

    public async Task<ChatEntity?> GetById(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entity = await dbSet.FirstOrDefaultAsync(c => c.Id == id && c.DateDeleted == null);
            return entity;
        });
    }
        
    public async Task<ChatEntity?> GetById(Guid id, bool isGroup)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entity = await dbSet.FirstOrDefaultAsync(c => c.IsGroup == isGroup && c.Id == id && c.DateDeleted == null);
            return entity;
        });
    }

    public async Task<ChatEntity?> GetByUserIds(Guid creatorId, Guid userWithId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entity = await dbSet.FirstOrDefaultAsync(c =>
                c.IsGroup == false && c.DateDeleted == null &&
                c.ChatUsers.Any(cu => cu.UserId == creatorId) &&
                c.ChatUsers.Any(cu => cu.UserId == userWithId));
            return entity;
        });
    }

    public async Task<ChatEntity?> Remove(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var chat = await dbSet.FirstOrDefaultAsync(c => c.Id == id);
            if (chat == null)
            {
                return null;
            }

            var entry = dbSet.Remove(chat);
            return entry.Entity;
        });
    }
}