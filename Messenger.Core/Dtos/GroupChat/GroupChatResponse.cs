﻿using System;
using System.Collections.Generic;
using Messenger.Core.Dtos.GroupChatUser;

namespace Messenger.Core.Dtos.GroupChat;

public class GroupChatResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Guid CreatorId { get; set; }
    public DateTime DateCreated { get; set; }
    public bool IsGroup { get; set; }
    public List<GroupChatUserResponse> ChatUsers { get; set; }
}