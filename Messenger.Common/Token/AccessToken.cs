﻿namespace Messenger.Common;

public class AccessToken
{
    public string Token { get; set; }
    public string RefreshToken { get; set; }
    public long Expiration { get; set; }
}