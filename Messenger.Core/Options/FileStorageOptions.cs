﻿namespace Messenger.Core.Options;

public class FileStorageOptions
{
    public string FileBasePath { get; set; }
}