﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.Message;
using Messenger.Database.Entities;

namespace Messenger.Core.Contracts.Services;

public interface IMessageService
{
    Task<MessageEntity> Add(Guid chatUserId, string? messageText, Guid? referenceMessageId = null);
    Task<MessageResponse?> Update(MessageUpdateRequest request);
    Task<LastMessageInChat?> GetLastByChatId(Guid chatId);
    Task<MessageResponse?> GetById(Guid id);
}