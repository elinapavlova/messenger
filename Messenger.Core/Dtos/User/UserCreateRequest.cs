﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.User;

public class UserCreateRequest
{
    public UserEntity User;

    public UserCreateRequest(UserEntity user)
    {
        User = user;
    }
}