﻿using Messenger.Common;
using Messenger.Core.Dtos.User;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class AuthMapper
{
    public static UserLoginResponse Map(UserResponse user, AccessToken token)
    {
        return new UserLoginResponse
        {
            Id = user.Id,
            Phone = user.Phone,
            UserName = user.UserName,
            AccessToken = token
        };
    }
}