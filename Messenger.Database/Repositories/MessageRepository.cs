﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Common.Helpers;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Repositories;

public class MessageRepository : BaseRepository<MessageEntity, Guid>, IMessageRepository
{
    public MessageRepository(AppDbContext context) : base(context)
    {
    }

    public async Task<MessageEntity?> Add(MessageEntity message, bool hasFile = false)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            message.Text = message.Text?.Trim();
            if (hasFile is false)
            {
                if (MessageHelper.IsTextValid(message.Text) is false && message.ReferenceMessageId is null)
                {
                    return null;
                }
            }
            message.DateCreated = DateTime.UtcNow;
            var entry = await dbSet.AddAsync(message);
            return entry.Entity;
        });
    }

    public async Task<MessageEntity?> Update(MessageEntity updatedMessage)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            updatedMessage.Text = updatedMessage.Text?.Trim();
            if (MessageHelper.IsTextValid(updatedMessage.Text) is false && updatedMessage.ReferenceMessageId is null)
            {
                return null;
            }

            var message = await dbSet.FirstOrDefaultAsync(m => m.Id == updatedMessage.Id && m.DateDeleted == null);
            if (message == null || message.ChatUserId != updatedMessage.ChatUserId)
            {
                return null;
            }

            message.Text = updatedMessage.Text;
            message.DateUpdated = DateTime.UtcNow;
            var entry = dbSet.Update(message);
            return entry.Entity;
        });
    }

    public async Task<MessageEntity?> Remove(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var message = await dbSet.FirstOrDefaultAsync(m => m.Id == id && m.DateDeleted == null);
            if (message == null)
            {
                return null;
            }

            message.DateDeleted = DateTime.UtcNow;
            var entry = dbSet.Update(message);
            return entry.Entity;
        });
    }
        
    public async Task<MessageEntity?> GetById(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.FirstOrDefaultAsync(m => m.Id == id);
        });
    }
        
    public async Task<List<MessageEntity>?> GetByChatId(Guid chatId, Filter filter)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entities = dbSet
                .Where(m => m.ChatId == chatId && m.DateDeleted == null)
                .OrderByDescending(m => m.DateCreated);
           
            return await GetFiltered(entities, filter);
        });
    }
        
    public async Task<MessageEntity?> GetLastByChatId(Guid chatId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entity = await dbSet
                .Where(m => m.ChatId == chatId && m.DateDeleted == null)
                .OrderByDescending(m => m.DateCreated)
                .FirstOrDefaultAsync();
            return entity;
        });
    }
}