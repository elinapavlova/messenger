﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.User;
using Messenger.Core.Mappers;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;

namespace Messenger.Core.Services;

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;
        
    public UserService
    (
        IUserRepository userRepository
    )
    {
        _userRepository = userRepository;
    }

    public async Task<UserCreateResponse?> Add(UserCreateRequest request)
    {
        var user = await _userRepository.GetByPhone(request.User.Phone);
        if (user is not null)
        {
            return null;
        }
        user = await _userRepository.Add(new UserEntity
        {
            UserName = request.User.UserName, Phone = request.User.Phone
        });

        return user is null 
            ? null 
            : UserMapper.MapCreatedResponse(user);
    }

    public async Task<UserResponse?> GetByPhone(string phone)
    {
        var user = await _userRepository.GetByPhone(phone);
        return user is null 
            ? null
            : UserMapper.MapResponse(user);
    }
        
    public async Task<UserResponse?> GetById(Guid id)
    {
        var user = await _userRepository.GetById(id);
        return user is null 
            ? null
            : UserMapper.MapResponse(user);
    }

    public async Task<UserResponse?> Update(UserUpdateRequest request)
    {
        var user = await _userRepository.Update(request.User);
        return user is null 
            ? null
            : UserMapper.MapResponse(user);
    }

    public async Task<UserResponse?> Remove(Guid id)
    {
        var user = await _userRepository.Remove(id);
        return user is null
            ? null
            : UserMapper.MapResponse(user);
    }
}