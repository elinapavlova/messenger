﻿using System;
using System.Threading.Tasks;
using Messenger.Database.Entities;

namespace Messenger.Core.Contracts.Services;

public interface IChatUserContactNameService
{
    Task<ChatUserEntity> GetChatUserWithContactName(Guid chatId, Guid userId, Guid userContactId);
}