﻿using Microsoft.Extensions.Configuration;

namespace Messenger.Common.Helpers;

public static class ConfigurationHelper
{
    public static T GetOptions<T>(this IConfiguration configuration, string key)
        where T : new()
    {
        var value = new T();
        configuration.Bind(key, value);
        return value;
    }
}