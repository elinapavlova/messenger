﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Messenger.Database.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Repositories;

public class RefreshTokenRepository : BaseRepository<RefreshTokenEntity, Guid>, IRefreshTokenRepository
{
    public RefreshTokenRepository(AppDbContext context) : base(context)
    {
    }

    public async Task<List<RefreshTokenEntity>> GetByUserId(Guid userId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.Where(x => 
                    x.DateExpired > DateTime.UtcNow &&
                    x.DateUsed == null &&
                    x.UserId == userId)
                .ToListAsync();
        });
    }

    public async Task<RefreshTokenEntity> GetByRefreshToken(string token)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            if (TokenHelper.IsTokenValid(token) is false)
            {
                return null;
            }
            return await dbSet.FirstOrDefaultAsync(x => 
                x.DateExpired >= DateTime.UtcNow &&
                x.DateUsed == null &&
                x.Token == token);
        });
    }

    public async Task<RefreshTokenEntity> Update(RefreshTokenEntity updatedRefreshToken)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var refreshToken = await dbSet.FirstOrDefaultAsync(t => t.Token == updatedRefreshToken.Token);
            if (refreshToken is null)
            {
                return null;
            }
            refreshToken.DateCreated = updatedRefreshToken.DateCreated;
            refreshToken.DateExpired = updatedRefreshToken.DateExpired;
                
            var entry = dbSet.Update(refreshToken);
            return entry.Entity;
        });
    }

    public async Task<List<RefreshTokenEntity>> UpdateList(List<RefreshTokenEntity> updatedRefreshTokens)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var refreshTokens = new List<RefreshTokenEntity>();
            foreach (var updatedRefreshToken in updatedRefreshTokens)
            {
                var refreshToken = await dbSet.FirstOrDefaultAsync(t => t.Token == updatedRefreshToken.Token);
                if (refreshToken is null)
                {
                    return null;
                }
                refreshToken.DateCreated = updatedRefreshToken.DateCreated;
                refreshToken.DateExpired = updatedRefreshToken.DateExpired;
                    
                var entry = dbSet.Update(refreshToken);
                refreshTokens.Add(entry.Entity);
            }
            return refreshTokens;
        });
    }

    public async Task<RefreshTokenEntity> Add(RefreshTokenEntity refreshToken)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            if (TokenHelper.IsTokenValid(refreshToken.Token) is false || 
                TokenHelper.RefreshTokenFilter(refreshToken.DateExpired, refreshToken.DateUsed) is false)
            {
                return null;
            }
            var entry = await dbSet.AddAsync(refreshToken);
            return entry.Entity;
        });
    }
}