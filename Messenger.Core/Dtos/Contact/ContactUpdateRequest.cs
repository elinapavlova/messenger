﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.Contact;

public class ContactUpdateRequest
{
    public readonly ContactEntity Contact;

    public ContactUpdateRequest(ContactEntity contact)
    {
        Contact = contact;
    }
}