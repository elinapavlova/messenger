﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Contracts;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos;
using Messenger.Core.Mappers;

namespace Messenger.Core.Services;

public class GroupChatMessageService : IGroupChatMessageService
{
    private readonly IMessageFileService _messageService;
    private readonly IGroupChatService _groupChatService;

    public GroupChatMessageService(IMessageFileService messageService, IGroupChatService groupChatService)
    {
        _messageService = messageService;
        _groupChatService = groupChatService;
    }
    
    public async Task<GroupChatMessagesResponse?> GetGroupChatByIdWithMessages(Guid id, FilterDto filter)
    {
        var chat = await _groupChatService.GetById(id);
        if (chat == null)
        {
            return null;
        }
        var messages = await _messageService.GetMessagesByChatIdWithFiles(chat.Id, filter);
        return GroupChatMapper.MapGroupChatMessages(chat, messages);
    }
}