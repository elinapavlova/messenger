﻿using System;

namespace Messenger.Api.Dtos;

public record ChatEntryByInvitationRequestDto
{
    public Guid UserId { get; set; }
}