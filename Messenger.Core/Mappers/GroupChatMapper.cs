﻿#nullable enable
using System.Collections.Generic;
using System.Linq;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.GroupChat;
using Messenger.Core.Dtos.GroupChatUser;
using Messenger.Core.Dtos.Message;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class GroupChatMapper
{
    public static GroupChatCreateResponse MapCreatedResponse(ChatEntity chat)
    {
        var users = MapGroupChatUsersResponse(chat);
            
        return new GroupChatCreateResponse
        {
            Id = chat.Id, 
            Name = chat.Name, 
            CreatorId = chat.CreatorId, 
            DateCreated = chat.DateCreated, 
            Users = users
        };
    }

    public static GroupChatResponse MapResponse(ChatEntity chat)
    {
        var users = MapGroupChatUsersResponse(chat);
            
        return new GroupChatResponse
        {
            Id = chat.Id, 
            Name = chat.Name, 
            CreatorId = chat.CreatorId, 
            DateCreated = chat.DateCreated, 
            ChatUsers = users
        };
    }

    private static List<GroupChatUserResponse>? MapGroupChatUsersResponse(ChatEntity chat)
    {
        return chat.ChatUsers?.Select(MapGroupChatUserResponse).ToList();
    }

    public static GroupChatMessagesResponse MapGroupChatMessages(GroupChatResponse chat, List<MessageResponseWithFiles>? messages)
    {
        return new GroupChatMessagesResponse
        {
            Id = chat.Id, Name = chat.Name, 
            ChatUsers = chat.ChatUsers, 
            DateCreated = chat.DateCreated, 
            Messages = messages
        };
    }
        
    public static ChatEntity Map(GroupChatResponse chat)
    {
        var users = MapGroupChatUsers(chat);
            
        return new ChatEntity
        {
            Id = chat.Id, 
            Name = chat.Name, 
            CreatorId = chat.CreatorId, 
            DateCreated = chat.DateCreated, 
            ChatUsers = users,
            IsGroup = chat.IsGroup
        };
    }
        
    private static List<ChatUserEntity>? MapGroupChatUsers(GroupChatResponse chat)
    {
        return chat.ChatUsers?.Select(MapGroupChatUser).ToList();
    }
        
    private static ChatUserEntity MapGroupChatUser(GroupChatUserResponse chatUser)
    {
        return new ChatUserEntity
        {
            Id = chatUser.Id,
            ChatId = chatUser.ChatId,
            UserId = chatUser.UserId,
            DateStarted = chatUser.DateStarted
        };
    }

    private static GroupChatUserResponse MapGroupChatUserResponse(ChatUserEntity chatUser)
    {
        return new GroupChatUserResponse
        {
            Id = chatUser.Id,
            ChatId = chatUser.ChatId,
            UserId = chatUser.UserId,
            DateStarted = chatUser.DateStarted
        };
    }
}