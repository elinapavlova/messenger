﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.Chat;

public class ChatCreateRequest
{
    public readonly UserEntity UserCreate;
    public readonly UserEntity UserWith;

    public ChatCreateRequest(UserEntity userCreate, UserEntity userWith)
    {
        UserCreate = userCreate;
        UserWith = userWith;
    }
}