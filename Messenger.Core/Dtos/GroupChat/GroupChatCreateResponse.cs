﻿using System;
using System.Collections.Generic;
using Messenger.Core.Dtos.GroupChatUser;

namespace Messenger.Core.Dtos.GroupChat;

public class GroupChatCreateResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Guid CreatorId { get; set; }
    public DateTime DateCreated { get; set; }
        
    public List<GroupChatUserResponse> Users { get; set; }
}