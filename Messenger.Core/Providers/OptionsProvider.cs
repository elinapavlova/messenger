﻿using Messenger.Core.Contracts.Provider;
using Messenger.Common.Helpers;
using Messenger.Core.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Messenger.Core.Providers;

public class OptionsProvider : IDependencyProviderWithConfiguration
{
    public void Register(IServiceCollection services, IConfiguration configuration)
    {
        var fileStorageOptions = configuration.GetOptions<FileStorageOptions>("FileStorage");
        var pagingOptions = configuration.GetOptions<FilterOptions>("FilterSettings");
        var appOptions = configuration.GetOptions<AppOptions>("App");
        var staticFilesOptions = configuration.GetOptions<StaticFilesOptions>("StaticFiles");
        services.AddSingleton(fileStorageOptions);
        services.AddSingleton(pagingOptions);
        services.AddSingleton(appOptions);
        services.AddSingleton(staticFilesOptions);
        services.AddScoped<PagingOptions>();
    }
}