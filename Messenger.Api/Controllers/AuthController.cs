﻿using System;
using System.Threading.Tasks;
using Messenger.Api.Dtos;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.User;
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Messenger.Api.Controllers;

[ApiVersion("1.0")]
[ApiController]
[EnableCors("CustomCorsPolicy")]
[Route("/api/v{version:apiVersion}/[controller]")]
public class AuthController : Controller
{
    private readonly IUserService _userService;
    private readonly IAuthService _authService;
    private readonly ITokenService _tokenService;

    public AuthController(IUserService userService, IAuthService authService, ITokenService tokenService)
    {
        _userService = userService;
        _authService = authService;
        _tokenService = tokenService;
    }
        
    [HttpPost("[action]")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<UserCreateResponse>> Register(UserCreateRequestDto dto)
    {
        var user = await _userService.GetByPhone(dto.Phone);
        if (user is not null)
        {
            return BadRequest("User with this phone number is already exist");
        }
            
        var result = await _authService.Register(new UserCreateRequest(new UserEntity
        {
            Phone = dto.Phone, UserName = dto.UserName
        }));
        if (result is null)
        {
            return BadRequest("Phone or UserName is not valid");
        }

        return Ok(result);
    }

    [HttpPost("[action]")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<UserLoginResponse>> Login(string phone)
    {
        var result = await _authService.Login(phone);
        if (result is null)
        {
            return NotFound("Phone number is not valid");
        }

        return Ok(result);
    }
        
    [HttpPost("refresh-token")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<UserLoginResponse>> RefreshToken(string refreshToken)
    {
        var result = await _authService.RefreshToken(refreshToken);
        if (result is null)
        {
            return BadRequest("Refresh token is not valid");
        }

        return Ok(result);
    }

    [HttpPost("[action]")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> Logout(Guid userId)
    {
        var user = await _userService.GetById(userId);
        if (user is null)
        {
            return BadRequest("User doesn't exist");
        }

        await _tokenService.ExitUserSessions(userId);
        return Ok();
    }
}