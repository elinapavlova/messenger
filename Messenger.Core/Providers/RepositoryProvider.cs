﻿using Messenger.Core.Contracts.Provider;
using Messenger.Database.Contracts;
using Messenger.Database.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Messenger.Core.Providers;

public class RepositoryProvider : IDependencyProvider
{
    public void Register(IServiceCollection services)
    {
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IChatRepository, ChatRepository>();
        services.AddScoped<IChatUserRepository, ChatUserRepository>();
        services.AddScoped<IMessageRepository, MessageRepository>();
        services.AddScoped<IContactRepository, ContactRepository>();
        services.AddScoped<IFileRepository, FileRepository>();
        services.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();
        services.AddScoped<IInvitationRepository, InvitationRepository>();
    }
}