﻿#nullable enable
namespace Messenger.Common.Helpers;

public static class ContactHelper
{
    public static bool IsContactNameValid(string? contactName)
    {
        return !string.IsNullOrEmpty(contactName) && 
               !string.IsNullOrWhiteSpace(contactName) && 
               contactName.Length is > 0 and <= 50;
    }
}