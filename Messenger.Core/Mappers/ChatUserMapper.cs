﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using Messenger.Core.Dtos.ChatUser;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class ChatUserMapper
{
    public static List<ChatUserEntity> Map(Guid chatId, IEnumerable<UserEntity> users)
    {
        return users.Select(user => new ChatUserEntity {ChatId = chatId, UserId = user.Id}).ToList();
    }

    public static List<ChatUserEntity> Map(Guid chatId, IEnumerable<ChatUserResponse> chatUsers)
    {
        return chatUsers.Select(cu => Map(chatId, cu)).ToList();
    }
        
    public static ChatUserEntity Map(ChatUserResponse chatUser)
    {
        return new ChatUserEntity
        {
            Id = chatUser.Id, ChatId = chatUser.ChatId, UserId = chatUser.UserId,
            DateStarted = chatUser.DateStarted, Name = chatUser.Name
        };
    }
        
    private static ChatUserEntity Map(Guid chatId, ChatUserResponse chatUser)
    {
        return new ChatUserEntity
        {
            Id = chatUser.Id, 
            ChatId = chatId, 
            UserId = chatUser.UserId, 
            Name = chatUser.Name, 
            DateStarted = chatUser.DateStarted
        };
    }
        
    public static List<ChatUserEntity> Map(IEnumerable<ChatUserResponse> items)
    {
        return items.Select(Map).ToList();
    }
        
    public static ChatUserResponse MapResponse(ChatUserEntity chatUser)
    {
        return new ChatUserResponse
        {
            Id = chatUser.Id, 
            ChatId = chatUser.ChatId, 
            UserId = chatUser.UserId, 
            Name = chatUser.Name, 
            DateStarted = chatUser.DateStarted
        };
    }

    private static ChatUserResponse MapResponse(Guid chatId, ChatUserEntity chatUser)
    {
        return new ChatUserResponse
        {
            Id = chatUser.Id, 
            ChatId = chatId, 
            UserId = chatUser.UserId, 
            Name = chatUser.Name, 
            DateStarted = chatUser.DateStarted
        };
    }
        
    public static List<ChatUserResponse> MapResponse(IEnumerable<ChatUserEntity> items)
    {
        return items.Select(MapResponse).ToList();
    }
        
    public static List<ChatUserResponse> MapResponse(Guid chatId, IEnumerable<ChatUserEntity> chatUsers)
    {
        return chatUsers.Select(cu => MapResponse(chatId, cu)).ToList();
    }
}