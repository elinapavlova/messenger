﻿#nullable enable
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Common.Helpers;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.File;
using Messenger.Core.Dtos.Message;
using Messenger.Core.Logger;
using Messenger.Core.Mappers;
using Messenger.Core.Options;
using Messenger.Database.Contracts;
using Microsoft.AspNetCore.Http;

namespace Messenger.Core.Services;

public class FileService : IFileService
{
    private readonly IFileRepository _fileRepository;
    private readonly IChatRepository _chatRepository;
    private readonly IMessageService _messageService;
    private readonly ILogger _logger;
    private readonly string _fileBasePath;
    private readonly Uri _fileBaseUrl;
    private readonly PagingOptions _pagingOptions;

    public FileService
    (
        IFileRepository fileRepository,
        IChatRepository chatRepository,
        IMessageService messageService,
        ILogger logger,
        FileStorageOptions fileStorageOptions,
        StaticFilesOptions staticFilesOptions,
        FilterOptions filterOptions
    )
    {
        _fileRepository = fileRepository;
        _chatRepository = chatRepository;
        _messageService = messageService;
        _logger = logger;
        _fileBasePath = fileStorageOptions.FileBasePath;
        _fileBaseUrl = new Uri(staticFilesOptions.BaseUrl);
        _pagingOptions = filterOptions.Filters["Files"] ?? filterOptions.Filters["Default"];
    }
        
    public async Task<FilesUploadResponse?> Upload(FilesUploadRequest request)
    {
        if (request.Files.Count is <= 0 or > 10 || request.Files.Any(UploadFileFilter) || 
            await IsValidReferenceMessageFilter(request.ReferenceMessage?.Id) is false)
        {
            return null;
        }
        var files = new List<FileUpload>();
        var paths = new List<string>();
        var referenceMessage = new MessageResponse();
            
        await using var transaction = await _fileRepository.BeginTransaction();
        try
        {
            if (transaction is null)
            {
                throw new Exception("Failed start transaction");
            }

            if (request.ReferenceMessage?.Id is not null)
            {
                referenceMessage = await _messageService.GetById(request.ReferenceMessage.Id);
            }    
            var message = await _messageService.Add(request.ChatUser.Id, request.Text, referenceMessage.Id);
            var uploadModel = new UploadModel(message.ChatUser.ChatId, message.Id, message.ChatUser.Id,
                                              message.ChatUser.UserId, request.Files);
            await SaveFiles(uploadModel, paths, files);
                
            return new FilesUploadResponse
            {
                Files = files, Text = message.Text, MessageId = message.Id, ChatId = message.ChatUser.ChatId,
                ReferenceMessageId = referenceMessage?.Id
            };
        }
        catch (Exception exception)
        {
            _logger.Error(exception, exception.Message);
            _logger.Debug(exception, "Failed upload file");
            if (transaction is not null)
            {
                transaction.HasError = true;
            }
            FileHelper.RevertSavingFiles(paths);
            return null;
        }
    }

    public async Task<List<FileResponse>?> GetByMessageId(Guid messageId)
    {
        var result = await _fileRepository.GetByMessageId(messageId);
        return result is null
            ? null
            : FileMapper.MapResponse(result);
    }

    public async Task<FileResponse?> Remove(Guid id)
    {
        var result = await _fileRepository.Remove(id);
        if (result is null)
        {
            return null;
        }
        var isFileExist = FileHelper.IsFileExist(result.Path);
        if (isFileExist)
        {
            File.Delete(result.Path);
        }
        return FileMapper.MapResponse(result);
    }

    public Uri? CreateUrl(string path)
    {
        var basePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), _fileBasePath);
        if (path.StartsWith(basePath) is false)
        {
            return null;
        }
        path = Path.GetRelativePath(basePath, path);
        return new Uri(_fileBaseUrl, path);
    }

    public async Task<List<FileResponse>?> GetByChatId(Guid chatId, FilterDto filter)
    {
        var chat = await _chatRepository.GetById(chatId);
        if (chat is null)
        {
            return null;
        }

        var files = await _fileRepository.GetByChatId(chat.Id, FilterMapper.Map(filter, _pagingOptions));
        var result = files?.Select(file => FileMapper.MapResponse(file, CreateUrl(file.Path)))
            .OrderByDescending(f => f.DateCreated)
            .ToList();

        return result;
    }

    private static bool UploadFileFilter(IFormFile file)
    { 
        return !FileExtension.Extensions.ContainsKey(Path.GetExtension(file.FileName).ToLower()) 
               && FileHelper.IsFileNameValid(file.FileName) 
               && file.Length is > 0 and <= 104857600;
    }

    private async Task<bool> IsValidReferenceMessageFilter(Guid? id)
    {
        if (id is null)
        {
            return true;
        }

        var referenceMessage = await _messageService.GetById((Guid)id);
        return referenceMessage is not null;
    }

    private async Task SaveFiles(UploadModel uploadModel, ICollection<string> paths, ICollection<FileUpload> files)
    {
        foreach (var file in uploadModel.FormFiles)
        {
            var savedFile = await SaveFile(uploadModel.ChatId, uploadModel.ChatUserId, file);
            paths.Add(savedFile.Path);
                
            var mappedFile = FileMapper.Map(savedFile, uploadModel.MessageId, uploadModel.ChatId);
            var newFile = await _fileRepository.Create(mappedFile);
            if (newFile is null)
            {
                throw new Exception($"Failed upload file {file.FileName} by user {uploadModel.UserId}");
            }

            savedFile.Id = newFile.Id;
            files.Add(savedFile);
        }
    }
        
    private async Task<FileUpload> SaveFile(Guid chatId, Guid chatUserId, IFormFile file)
    {
        var absolutePath = FileHelper.CreateAbsolutePath
            (Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), _fileBasePath, chatId, file.FileName);
        if (absolutePath is null)
        {
            throw new Exception($"Failed upload file {file.FileName} by user {chatUserId}\r\n" +
                                "File has unexpected file extension");
        }
        var directoryName = Path.GetDirectoryName(absolutePath);
        FileHelper.CheckDirectory(directoryName);

        await using var fileStream = File.Create(absolutePath);
        await file.CopyToAsync(fileStream);

        return new FileUpload
        {
            Name = Path.GetFileName(absolutePath), 
            Path = absolutePath, 
            Extension = Path.GetExtension(absolutePath),
            Url = CreateUrl(absolutePath)
        };
    }

    private class UploadModel
    {
        public readonly Guid ChatId;
        public readonly Guid MessageId;
        public readonly Guid ChatUserId;
        public readonly Guid UserId;
        public readonly IFormFileCollection FormFiles;

        public UploadModel(Guid chatId, Guid messageId, Guid chatUserId, Guid userId, IFormFileCollection files)
        {
            ChatId = chatId;
            MessageId = messageId;
            ChatUserId = chatUserId;
            UserId = userId;
            FormFiles = files;
        }
    }
}