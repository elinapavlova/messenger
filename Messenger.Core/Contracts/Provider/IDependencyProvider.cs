﻿using Microsoft.Extensions.DependencyInjection;

namespace Messenger.Core.Contracts.Provider;

public interface IDependencyProvider
{
    void Register(IServiceCollection services);
}