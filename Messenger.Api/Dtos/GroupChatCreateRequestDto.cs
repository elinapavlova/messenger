﻿#nullable enable
using System;
using System.Collections.Generic;

namespace Messenger.Api.Dtos;

public record GroupChatCreateRequestDto
{
    public string? Name { get; set; }
    public Guid CreatorId { get; set; }
    public List<Guid>? UserIds { get; set; }
}