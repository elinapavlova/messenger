﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Database.Entities;

namespace Messenger.Database.Contracts;

public interface IMessageRepository : IBaseRepository<MessageEntity, Guid>
{
    Task<MessageEntity?> Add(MessageEntity message, bool hasFile = false);
    Task<MessageEntity?> Update(MessageEntity updatedMessage);
    Task<MessageEntity?> Remove(Guid id);
    Task<MessageEntity?> GetById(Guid id);
    Task<List<MessageEntity>?> GetByChatId(Guid chatId, Filter filter);
    Task<MessageEntity?> GetLastByChatId(Guid chatId);
}