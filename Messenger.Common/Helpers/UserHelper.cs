﻿#nullable enable
namespace Messenger.Common.Helpers;

public static class UserHelper
{
    public static bool IsPhoneValid(string? phone)
    {
        if (string.IsNullOrEmpty(phone) || phone.Length is < 10 or > 20)
        {
            return false;
        }
        return phone.All(x => !char.IsLetter(x));
    }

    public static bool IsUserNameValid(string? userName)
    {
        return !string.IsNullOrEmpty(userName) && 
               !string.IsNullOrWhiteSpace(userName) && 
               userName.Length is > 0 and <= 50;
    }
}