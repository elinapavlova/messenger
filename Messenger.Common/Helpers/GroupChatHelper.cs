﻿#nullable enable
namespace Messenger.Common.Helpers;

public static class GroupChatHelper
{
    public static bool IsGroupChatNameValid(string? groupChatName)
    {
        return !string.IsNullOrEmpty(groupChatName) && 
               !string.IsNullOrWhiteSpace(groupChatName) && 
               groupChatName.Length is > 0 and <= 50;
    }
}