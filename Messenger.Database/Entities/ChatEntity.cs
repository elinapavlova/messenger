﻿using System;
using System.Collections.Generic;

namespace Messenger.Database.Entities;

public class ChatEntity : BaseEntity<Guid>
{
    public string Name { get; set; }
    public bool IsGroup { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime? DateDeleted { get; set; }
    public Guid CreatorId { get; set; }

    public List<ChatUserEntity> ChatUsers { get; set; }
    public List<MessageEntity> Messages { get; set; }
    public List<FileEntity> Files { get; set; }
    public List<InvitationEntity> Invitations { get; set; }
    public UserEntity User { get; set; }
}