﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.GroupChat;
using Messenger.Core.Logger;
using Messenger.Core.Mappers;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;

namespace Messenger.Core.Services;

public class GroupChatService : IGroupChatService
{
    private readonly IChatUserService _chatUserService;
    private readonly IChatRepository _chatRepository;
    private readonly IUserRepository _userRepository;
    private readonly ILogger _logger;

    public GroupChatService
    (
        IChatUserService chatUserService,
        IChatRepository chatRepository,
        IUserRepository userRepository,
        ILogger logger
    )
    {
        _chatUserService = chatUserService;
        _chatRepository = chatRepository;
        _userRepository = userRepository;
        _logger = logger;
    }

    public async Task<GroupChatCreateResponse?> Add(GroupChatCreateRequest? request)
    {
        request?.Users?.Add(new UserEntity { Id = request.GroupChat.CreatorId });
        if (await IsAddRequestValid(request) is false)
        {
            return null;
        }
        await using var transaction = await _chatRepository.BeginTransaction();
        try
        {
            if (transaction is null)
            {
                throw new Exception("Failed start transaction");
            }
                
            var chat = await CreateGroupChat(request.GroupChat);
            var chatUsers = await CreateGroupChatUsers(chat.Id, request.GroupChat.CreatorId, request.Users);
            chat.ChatUsers = chatUsers;
                
            return GroupChatMapper.MapCreatedResponse(chat);
        }
        catch(Exception exception)
        {
            _logger.Error(exception.Message);
            if (transaction is not null)
            {
                transaction.HasError = true;
            }
            return null;
        }
    }

    public async Task<GroupChatResponse?> GetById(Guid id)
    {
        var chat = await _chatRepository.GetById(id, true);
        if (chat == null)
        {
            return null;
        }

        chat.ChatUsers = await GetGroupChatUsersByChatId(chat.Id);
        return GroupChatMapper.MapResponse(chat);
    }
        
    public async Task<GroupChatResponse?> Update(GroupChatUpdateRequest request)
    {
        var chat = await _chatRepository.Update(new ChatEntity
        {
            Id = request.GroupChat.Id, Name = request.GroupChat.Name
        });
        return chat is null 
            ? null
            : GroupChatMapper.MapResponse(chat);
    }

    public async Task<GroupChatResponse?> Remove(Guid id)
    {
        var chat = await _chatRepository.Remove(id);
        return chat is null 
            ? null 
            : GroupChatMapper.MapResponse(chat);
    }

    public async Task<bool> IsGroupChatExist(Guid id)
    {
        return await _chatRepository.IsChatExist(id);
    }

    private async Task<bool> IsAddRequestValid(GroupChatCreateRequest? request)
    {
        var userIds = request?.Users?.Select(u => u.Id).Distinct().ToList();
            
        if (request?.GroupChat.CreatorId is null || request.Users?.Count != userIds?.Count || 
            userIds?.Count is null or <= 1)
        {
            return false;
        }

        var users = await GetUsersByIds(userIds);
        return users is not null;
    }

    private async Task<List<UserEntity>?> GetUsersByIds(List<Guid> userIds)
    {
        var users = new List<UserEntity>();
        foreach (var id in userIds)
        {
            var user = await GetUserById(id);
            if (user is null)
            {
                return null;
            }
            users.Add(user);
        }
        return users;
    }

    private async Task<UserEntity?> GetUserById(Guid userId)
    { 
        return await _userRepository.GetById(userId);
    }

    private async Task<ChatEntity> CreateGroupChat(ChatEntity groupChat)
    {
        var chat = await _chatRepository.Add(groupChat);
        if (chat is not null)
        {
            return chat;
        }
        throw new Exception($"Failed create group chat by user Id {groupChat.CreatorId} with name {groupChat.Name}");
    }

    private async Task<List<ChatUserEntity>?> CreateGroupChatUsers(Guid chatId, Guid creatorId,
        IReadOnlyCollection<UserEntity> users)
    {
        try
        {
            var chatUsers = await _chatUserService.AddList(ChatUserMapper.Map(chatId, users));
            if (chatUsers is not null)
            {
                return chatUsers;
            }

            var text = CreateErrorText(users, creatorId);
            throw new Exception(text);
        }
        catch (Exception exception)
        {
            _logger.Error(exception.Message);
            throw new Exception(CreateErrorText(users, creatorId));
        }
    }

    private async Task<List<ChatUserEntity>?> GetGroupChatUsersByChatId(Guid id)
    { 
        var chatUsers = await _chatUserService.GetByChatId(id);
        return chatUsers is null
            ? null
            : ChatUserMapper.Map(chatUsers);
    }

    private static string CreateErrorText(IEnumerable<UserEntity> users, Guid creatorId)
    {
        var text = users.Aggregate("Failed add users", (current, user) => current + $" {user.Id}");
        text += $" in a new group chat created by user {creatorId}";
        return text;
    }
}