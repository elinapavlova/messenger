﻿using Messenger.Core.Dtos.Contact;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class ContactMapper
{
    public static ContactCreateResponse MapCreatedResponse(ContactEntity contact)
    {
        return new ContactCreateResponse
        {
            Id = contact.Id, 
            UserId = contact.UserId, 
            ContactId = contact.ContactUserId,
            ContactName = contact.ContactName, 
            DateCreated = contact.DateCreated
        };
    }

    public static ContactResponse MapResponse(ContactEntity contact)
    {
        return new ContactResponse
        {
            Id = contact.Id, 
            UserId = contact.UserId, 
            ContactUserId = contact.ContactUserId,
            ContactName = contact.ContactName
        };
    }
}