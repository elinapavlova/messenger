﻿using System;

namespace Messenger.Api.Dtos;

public record ContactCreateRequestDto
{
    public Guid UserId { get; set; }
    public Guid ContactUserId { get; set; }
    public string ContactName { get; set; }
}