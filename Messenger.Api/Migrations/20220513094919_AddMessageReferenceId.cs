﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Messenger.Api.Migrations
{
    public partial class AddMessageReferenceId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ReferenceMessageId",
                table: "Messages",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferenceMessageId",
                table: "Messages");
        }
    }
}
