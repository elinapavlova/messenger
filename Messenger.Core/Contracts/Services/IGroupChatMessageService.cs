﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Dtos;

namespace Messenger.Core.Contracts.Services;

public interface IGroupChatMessageService
{
    Task<GroupChatMessagesResponse?> GetGroupChatByIdWithMessages(Guid id, FilterDto filter);
}