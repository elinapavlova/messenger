﻿#nullable enable
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Messenger.Core.Dtos.File;

public class FilesUploadResponse
{
    public List<FileUpload>? Files { get; set; }
    public string? Text { get; set; }
    public Guid MessageId { get; set; }
    public Guid ChatId { get; set; }
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public Guid? ReferenceMessageId { get; set; }
}