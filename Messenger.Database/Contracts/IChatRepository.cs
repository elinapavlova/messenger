﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Database.Entities;

namespace Messenger.Database.Contracts;

public interface IChatRepository : IBaseRepository<ChatEntity, Guid>
{
    Task<ChatEntity?> Add(UserEntity? userCreate, UserEntity? userWith);
    Task<ChatEntity?> Add(ChatEntity chat);
    Task<ChatEntity?> Update(ChatEntity updatedChat);
    Task<bool> IsChatExist(Guid creatorId, Guid userWithId);
    Task<bool> IsChatExist(Guid id);
    Task<ChatEntity?> GetByUserIds(Guid creatorId, Guid userWithId);
    Task<ChatEntity?> GetById(Guid id);
    Task<ChatEntity?> GetById(Guid id, bool isGroup);
    Task<ChatEntity?> Remove(Guid id);
    Task<List<ChatEntity>?> GetByUserId(Guid userId, Filter filter);
}