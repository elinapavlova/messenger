﻿using System;

namespace Messenger.Core.Dtos.Invitation;

public class InvitationCreateResponse
{
    public Uri Url { get; set; }
}