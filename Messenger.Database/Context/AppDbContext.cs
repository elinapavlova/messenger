﻿using Messenger.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Context;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }
        
    public DbSet<UserEntity> Users { get; set; }
    public DbSet<ChatUserEntity> ChatUsers { get; set; }
    public DbSet<ChatEntity> Chats { get; set; }
    public DbSet<MessageEntity> Messages { get; set; }
    public DbSet<ContactEntity> Contacts { get; set; }
    public DbSet<FileEntity> Files { get; set; }
    public DbSet<RefreshTokenEntity> RefreshTokens { get; set; }
    public DbSet<InvitationEntity> Invitations { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<UserEntity>(user =>
        {
            user.Property(_ => _.Phone).IsRequired().HasMaxLength(20);
            user.Property(_ => _.UserName).IsRequired().HasMaxLength(50);
            user.Property(_ => _.DateCreated).IsRequired();
        });
        builder.Entity<UserEntity>().HasIndex(user => user.Phone).IsUnique();

        builder.Entity<ChatEntity>(chat =>
        {
            chat.Property(_ => _.DateCreated).IsRequired();
            chat.Property(_ => _.Name).HasMaxLength(50);
                
            chat.HasOne(_ => _.User)
                .WithMany(user => user.Chats)
                .HasForeignKey(_ => _.CreatorId);
        });

        builder.Entity<ChatUserEntity>(chatUser =>
        {
            chatUser.Property(_ => _.DateStarted).IsRequired();
            chatUser.Property(_ => _.Name).HasMaxLength(50);

            chatUser.HasOne(_ => _.Chat)
                .WithMany(chat => chat.ChatUsers)
                .HasForeignKey(_ => _.ChatId);
                
            chatUser.HasOne(_ => _.User)
                .WithMany(user => user.ChatUsers)
                .HasForeignKey(_ => _.UserId);
        });
        builder.Entity<ChatUserEntity>().HasIndex(chatUser => new {chatUser.ChatId, chatUser.UserId}).IsUnique();

        builder.Entity<MessageEntity>(message =>
        {
            message.Property(_ => _.Text).HasMaxLength(1000);
            message.Property(_ => _.DateCreated).IsRequired();

            message.HasOne(_ => _.ChatUser)
                .WithMany(chatUser => chatUser.Messages)
                .HasForeignKey(_ => _.ChatUserId);
                
            message.HasOne(_ => _.Chat)
                .WithMany(chat => chat.Messages)
                .HasForeignKey(_ => _.ChatId);
        });

        builder.Entity<ContactEntity>(contact =>
        {
            contact.Property(_ => _.ContactName).IsRequired().HasMaxLength(50);
            contact.Property(_ => _.DateCreated).IsRequired();

            contact.HasOne(_ => _.User)
                .WithMany(user => user.Contacts)
                .HasForeignKey(_ => _.UserId);

            contact.HasOne(_ => _.User)
                .WithMany(user => user.Contacts)
                .HasForeignKey(_ => _.ContactUserId);
        });

        builder.Entity<FileEntity>(file =>
        {
            file.Property(_ => _.Name).IsRequired().HasMaxLength(50);
            file.Property(_ => _.Path).IsRequired().HasMaxLength(200);
            file.Property(_ => _.Extension).IsRequired().HasMaxLength(5);
            file.Property(_ => _.DateCreated).IsRequired();

            file.HasOne(_ => _.Message)
                .WithMany(message => message.Files)
                .HasForeignKey(_ => _.MessageId);
            file.HasOne(_ => _.Chat)
                .WithMany(chat => chat.Files)
                .HasForeignKey(_ => _.ChatId);
        });

        builder.Entity<RefreshTokenEntity>(token =>
        {
            token.Property(_ => _.Token).IsRequired().HasMaxLength(300);
            token.Property(_ => _.Expiration).IsRequired();
            token.Property(_ => _.DateCreated).IsRequired();
            token.Property(_ => _.DateExpired).IsRequired();

            token.HasOne(_ => _.User)
                .WithMany(user => user.RefreshTokens)
                .HasForeignKey(_ => _.UserId);
        });

        builder.Entity<InvitationEntity>(invitation =>
        {
            invitation.Property(_ => _.DateCreated).IsRequired();
                
            invitation.HasOne(_ => _.ChatUser)
                .WithMany(chatUser => chatUser.Invitations)
                .HasForeignKey(_ => _.ChatUserId);
            invitation.HasOne(_ => _.Chat)
                .WithMany(chat => chat.Invitations)
                .HasForeignKey(_ => _.ChatId);
        });
    }
}