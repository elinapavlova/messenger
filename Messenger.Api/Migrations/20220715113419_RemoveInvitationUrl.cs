﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Messenger.Api.Migrations
{
    public partial class RemoveInvitationUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "Invitations");

            migrationBuilder.AddColumn<Guid>(
                name: "ChatId",
                table: "Invitations",
                type: "TEXT",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_ChatId",
                table: "Invitations",
                column: "ChatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Invitations_Chats_ChatId",
                table: "Invitations",
                column: "ChatId",
                principalTable: "Chats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invitations_Chats_ChatId",
                table: "Invitations");

            migrationBuilder.DropIndex(
                name: "IX_Invitations_ChatId",
                table: "Invitations");

            migrationBuilder.DropColumn(
                name: "ChatId",
                table: "Invitations");

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Invitations",
                type: "TEXT",
                maxLength: 150,
                nullable: false,
                defaultValue: "");
        }
    }
}
