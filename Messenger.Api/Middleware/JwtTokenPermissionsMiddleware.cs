﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using Messenger.Common;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Logger;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Messenger.Api.Middleware;

public class JwtTokenPermissionsMiddleware
{
    private readonly RequestDelegate _next;

    public JwtTokenPermissionsMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context, IUserService userService, IChatUserService chatUserService, 
        ITokenService tokenService, ILogger logger)
    {
        var userInfo = new UserTokenInfo();
        var path = context.Request.Path.Value;
        if (string.IsNullOrEmpty(path))
        {
            await _next(context);
            return;
        }

        if (string.IsNullOrEmpty(context.Request.Headers.Authorization) is false)
        {
            var token = context.Request.Headers.Authorization.ToString().Replace("Bearer ", string.Empty);
            userInfo = tokenService.GetUserInfoByToken(token);
        }

        if (path.Contains("/users"))
        {
            context.Request.EnableBuffering();
            using var stream = new StreamReader(context.Request.BodyReader.AsStream(true));
            var body = await stream.ReadToEndAsync();
            context.Request.Body.Position = 0;
            
            if (path.Contains(userInfo.Id.ToString()) is false && body.Contains(userInfo.Id.ToString()) is false)
            {
                logger.Warning("User by Id {userId} try to do {path}\r\nbody: {body}", userInfo.Id, path, body);
                context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                await context.Response.WriteAsync("You don't have permissions to request about another user");
                return;
            }
        }

        await _next(context);
    }
}

public static class JwtTokenPermissionsMiddlewareExtensions
{
    public static IApplicationBuilder UseTokenPermissions(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<JwtTokenPermissionsMiddleware>();
    }
}