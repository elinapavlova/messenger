﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Database.Entities;

namespace Messenger.Database.Contracts;

public interface IFileRepository : IBaseRepository<FileEntity, Guid>
{
    Task<FileEntity?> Remove(Guid id);
    Task<FileEntity?> Create(FileEntity file);
    Task<List<FileEntity>?> GetByMessageId(Guid messageId);
    Task<List<FileEntity>?> GetByChatId(Guid chatId, Filter filter);
}