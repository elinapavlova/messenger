﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.Contact;

public class ContactCreateRequest
{
    public readonly ContactEntity Contact;

    public ContactCreateRequest(ContactEntity contact)
    {
        Contact = contact;
    }
}