﻿namespace Messenger.Core.Options;

public class AppOptions
{
    public string Secret { get; set; }
    public string BaseUrl { get; set; }
    public int MaxCountOfActualInvitations { get; set; }
}