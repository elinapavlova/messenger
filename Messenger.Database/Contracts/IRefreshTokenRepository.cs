﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Database.Entities;

namespace Messenger.Database.Contracts;

public interface IRefreshTokenRepository
{
    Task<List<RefreshTokenEntity>> GetByUserId(Guid userId);
    Task<RefreshTokenEntity> GetByRefreshToken(string token);
    Task<RefreshTokenEntity> Update(RefreshTokenEntity updatedRefreshToken);
    Task<List<RefreshTokenEntity>> UpdateList(List<RefreshTokenEntity> updatedRefreshTokens);
    Task<RefreshTokenEntity> Add(RefreshTokenEntity refreshToken);
}