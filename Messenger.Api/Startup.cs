using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Messenger.Api.Middleware;
using Messenger.Common.Helpers;
using Messenger.Core.Options;
using Messenger.Core.Providers;
using Messenger.Database.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.OpenApi.Models;
using Serilog;

namespace Messenger.Api;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }
        
    public IConfiguration Configuration { get; set; }
        
    public void ConfigureServices(IServiceCollection services)  
    {
        ConfigureDbContext(services);
        ConfigureLogger();
        
        services.Register<LoggerProvider>();
        services.Register<OptionsProvider>(Configuration);
        services.Register<RepositoryProvider>();
        services.Register<ServicesProvider>();
        
        services.AddDirectoryBrowser();
        services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
        services.AddControllers();
           
        services.Register<AuthenticationProvider>(Configuration);
        ConfigureVersioning(services);
        ConfigureSwagger(services);

        services.AddCors(x => x.AddPolicy("CustomCorsPolicy", builder =>
        {
            builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        }));
    }

    private void ConfigureDbContext(IServiceCollection services)
    {
        var provider = Configuration.GetValue("Provider", "Default");
        services.AddDbContext<AppDbContext>(options =>
        {
            options.EnableSensitiveDataLogging();
            _ = provider switch
            {
                "SQLite" => options.UseSqlite(Configuration.GetConnectionString("SQLiteConnection"),
                    x => x.MigrationsAssembly("Messenger.Api")),
                "Default" => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"),
                    x => x.MigrationsAssembly("Messenger.PostgreSqlMigrations")),
                _ => throw new ArgumentOutOfRangeException(nameof(services))
            };
        }, ServiceLifetime.Transient);
    }

    private void ConfigureLogger()
    {
        var projectFolderPath = Path.GetDirectoryName(AppContext.BaseDirectory);
        var logPath = Path.Join(
            projectFolderPath,
            "logs",
            "log_info_.log");

        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(Configuration)
            .WriteTo.File(
                logPath,
                rollingInterval: RollingInterval.Day,
                retainedFileCountLimit: 7)
            .CreateLogger();
    }

    private static void ConfigureVersioning(IServiceCollection services)
    {
        services.AddApiVersioning(options =>
        {
            options.DefaultApiVersion = new ApiVersion(1, 0);
            options.AssumeDefaultVersionWhenUnspecified = true;
            options.ReportApiVersions = true;
            options.ApiVersionReader = new UrlSegmentApiVersionReader();
        });
        services.AddVersionedApiExplorer(options =>
        {
            options.GroupNameFormat = "'v'VVV";
            options.SubstituteApiVersionInUrl = true;
        });
    }

    private static void ConfigureSwagger(IServiceCollection services)
    {
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "Messenger.Api",
            });
            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Name = "Authorization",
                BearerFormat = "Bearer {authToken}",
                Description = "JSON Web Token to access resources. Example: Bearer {token}",
                Type = SecuritySchemeType.ApiKey
            });
            options.AddSecurityRequirement(
                new OpenApiSecurityRequirement {{
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme, Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }});
                
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            options.IncludeXmlComments(xmlPath);
        });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
    {
        app.UseDeveloperExceptionPage();
        app.UseSwagger();
        app.UseSwaggerUI(
            opt => {
                foreach (var description in provider.ApiVersionDescriptions) {
                    opt.SwaggerEndpoint(
                        $"/swagger/{description.GroupName}/swagger.json",
                        description.GroupName.ToUpperInvariant());
                }
            });

        ConfigureUsingStaticFiles(app);

        app.UseAuthentication();
        app.UseHttpsRedirection();
        app.UseRouting();
        app.UseCors("CustomCorsPolicy");
        app.UseAuthorization();
        app.UseTokenPermissions();
        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }

    private void ConfigureUsingStaticFiles(IApplicationBuilder app)
    {
        var staticFilesOptions = Configuration.GetOptions<StaticFilesOptions>("StaticFiles");
        var fileStorageOptions = Configuration.GetOptions<FileStorageOptions>("FileStorage");
        var cacheControl = staticFilesOptions.Headers.FirstOrDefault(header => header.Key == "Cache-Control").Value;
        
        var fileStoragePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
            fileStorageOptions.FileBasePath);
        if (Directory.Exists(fileStoragePath) is false)
        {
            Directory.CreateDirectory(fileStoragePath);
        }
        
        app.UseStaticFiles(new StaticFileOptions
        {
            OnPrepareResponse = ctx =>
            {
                ctx.Context.Response.Headers["Cache-Control"] = $"{cacheControl}";
            },
            FileProvider = new PhysicalFileProvider(fileStoragePath),
            RequestPath = "/files"
        });
    }
}