﻿#nullable enable
namespace Messenger.Common.Helpers;

public static class ChatUserHelper
{
    public static bool IsChatUserNameValid(string? chatUserName)
    {
        if (string.IsNullOrEmpty(chatUserName))
        {
            return true;
        }
        return !string.IsNullOrWhiteSpace(chatUserName) && 
               chatUserName.Length is > 0 and <= 50;
    }
}