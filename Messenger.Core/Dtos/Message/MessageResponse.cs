﻿using System;
using Newtonsoft.Json;

namespace Messenger.Core.Dtos.Message;

public class MessageResponse
{
    public Guid Id { get; set; }
    public string Text { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime? DateDeleted { get; set; }
    public DateTime? DateUpdated { get; set; }
    public Guid ChatUserId { get; set; }
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public ReferenceMessage ReferenceMessage { get; set; }
}