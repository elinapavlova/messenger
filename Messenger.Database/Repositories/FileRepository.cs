﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Common.Helpers;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Repositories;

public class FileRepository : BaseRepository<FileEntity, Guid>, IFileRepository
{
    public FileRepository(AppDbContext context) : base(context)
    {
    }

    public async Task<FileEntity?> Create(FileEntity file)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            if (FileHelper.IsFileNameValid(file.Name) is false)
            {
                return null;
            }

            file.DateCreated = DateTime.UtcNow;
            var entry = await dbSet.AddAsync(file);
            return entry.Entity;
        });
    }

    public async Task<FileEntity?> Remove(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var file = await dbSet.FirstOrDefaultAsync(f => f.Id == id);
            if (file is null)
            {
                return null;
            }

            var entry = dbSet.Remove(file);
            return entry.Entity;
        });
    }

    public async Task<List<FileEntity>?> GetByMessageId(Guid messageId, Filter filter)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entities = dbSet.Where(f => f.MessageId == messageId);
            return await GetFiltered(entities, filter);
        });
    }    
    
    public async Task<List<FileEntity>?> GetByMessageId(Guid messageId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            return await dbSet.Where(f => f.MessageId == messageId).ToListAsync();
        });
    }
        
    public async Task<List<FileEntity>?> GetByChatId(Guid chatId, Filter filter)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var entities = dbSet.Where(f => f.ChatId == chatId);
            return await GetFiltered(entities, filter);
        });
    }
}