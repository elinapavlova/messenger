﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Messenger.Database;

public abstract class BaseRepository<TModel, TKey> : IBaseRepository<TModel, TKey> 
    where TModel : BaseEntity<TKey>
{
    private readonly AppDbContext _context;
        
    protected BaseRepository(AppDbContext context)
    {
        _context = context;
    }

    protected async Task<TResult> ExecuteWithResult<TResult>(Func<DbSet<TModel>, Task<TResult>> func)
    {
        var dbSet = _context.Set<TModel>();
        var result = await func(dbSet);
        await _context.SaveChangesAsync();
        return result;
    }

    protected async Task<List<TEntity>> GetFiltered<TEntity, TFilter>(IQueryable<TEntity> source, TFilter filter)
        where TFilter : Filter
        where TEntity : BaseEntity<Guid>
    {
        var result = await source
            .Skip((filter.Paging.PageNumber - 1) * filter.Paging.PageSize)
            .Take(filter.Paging.PageSize).ToListAsync();
        return result;
    }

    public async Task<TransactionContainer?> BeginTransaction()
    {
        var contextTransaction = await _context.Database.BeginTransactionAsync();
        return Map(contextTransaction);
    }

    private static TransactionContainer Map(IDbContextTransaction contextTransaction, bool hasError = false)
    {
        return new TransactionContainer
        {
            ContextTransaction = contextTransaction, HasError = hasError
        };
    }
}