﻿namespace Messenger.Common.Helpers;

public static class FileExtension
{
    public static readonly Dictionary<string, string> Extensions = new ()
    {
        {".mp4", "video/mp4"},
        {".png", "image/png"},
        {".jpg", "image/jpeg"},
        {".jpeg", "image/jpeg"}
    }; 
}