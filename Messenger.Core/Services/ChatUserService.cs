﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.ChatUser;
using Messenger.Core.Dtos.GroupChat;
using Messenger.Core.Dtos.GroupChatUser;
using Messenger.Core.Mappers;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;

namespace Messenger.Core.Services;

public class ChatUserService : IChatUserService
{
    private readonly IChatUserRepository _chatUserRepository;

    public ChatUserService(IChatUserRepository chatUserRepository)
    {
        _chatUserRepository = chatUserRepository;
    }

    public async Task<ChatUserResponse?> GetById(Guid id)
    {
        var chatUser = await _chatUserRepository.GetById(id);
        return chatUser is null
            ? null
            : ChatUserMapper.MapResponse(chatUser);
    }

    public async Task<ChatUserResponse?> GetByChatIdUserId(Guid chatId, Guid userId)
    {
        var chatUser = await _chatUserRepository.GetByChatIdUserId(chatId, userId);
        return chatUser is null 
            ? null 
            : ChatUserMapper.MapResponse(chatUser);
    }

    public async Task<ChatUserResponse?> ComeOut(Guid chatUserId)
    {
        var chatUser = await _chatUserRepository.GetById(chatUserId);
        if (chatUser is null)
        {
            return null;
        }
        chatUser.DateComeOut = DateTime.UtcNow;
        chatUser = await _chatUserRepository.Update(chatUser);
            
        return chatUser is null
            ? null
            : ChatUserMapper.MapResponse(chatUser);
    }

    public async Task<List<ChatUserResponse>?> GetByChatId(Guid chatId)
    {
        var chatUsers = await _chatUserRepository.GetByChatId(chatId);
        return chatUsers is null 
            ? null 
            : ChatUserMapper.MapResponse(chatUsers);
    }
        
    public async Task<ChatUserResponse?> Add(ChatUserEntity chatUser)
    {
        var result = await _chatUserRepository.Add(chatUser);
        return result is null 
            ? null 
            : ChatUserMapper.MapResponse(result);
    }
        
    public async Task<List<ChatUserEntity>?> AddList(List<ChatUserEntity> chatUsers)
    {
        var result = await _chatUserRepository.AddList(chatUsers);
        return result;
    }
    
    public async Task<GroupChatResponse?> AddUserToGroupChat(GroupChatUserCreateRequest request)
    {
        var groupChat = request.GroupChat;
        var groupChatUser = await Add(new ChatUserEntity
        {
            ChatId = groupChat.Id, UserId = groupChat.CreatorId
        });
        if (groupChatUser == null)
        {
            return null;
        }

        var chatUsers = await GetByChatId(groupChat.Id);
        if (chatUsers is not null)
        {
            groupChat.ChatUsers = ChatUserMapper.Map(chatUsers);
        }
        return GroupChatMapper.MapResponse(groupChat);
    }
}