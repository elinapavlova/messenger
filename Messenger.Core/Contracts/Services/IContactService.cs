﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Dtos.Contact;

namespace Messenger.Core.Contracts.Services;

public interface IContactService
{
    Task<ContactCreateResponse?> Add(ContactCreateRequest request);
    Task<List<ContactResponse>?> GetByUserId(Guid userId, FilterDto filter);
    Task<ContactResponse?> GetById(Guid id);
    Task<ContactResponse?> GetByUserIdContactUserId(Guid userId, Guid contactUserId);
    Task<ContactResponse?> Update(ContactUpdateRequest request);
    Task<ContactResponse?> Remove(Guid id);
}