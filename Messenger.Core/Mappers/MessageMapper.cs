﻿#nullable enable
using System;
using System.Collections.Generic;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.Message;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class MessageMapper
{
    public static ReferenceMessage MapReferenceMessage(MessageEntity message, List<Uri>? files)
    {
        return new ReferenceMessage
        {
            Id = message.Id,
            ChatUserId = message.ChatUserId,
            ChatId = message.ChatId,
            DateCreated = message.DateCreated,
            DateDeleted = message.DateDeleted,
            Text = message.Text,
            Files = files
        };
    }

    public static MessageResponse MapResponse(MessageEntity message)
    {
        return new MessageResponse
        {
            Id = message.Id,
            ChatUserId = message.ChatUserId,
            DateCreated = message.DateCreated,
            DateDeleted = message.DateDeleted,
            DateUpdated = message.DateUpdated,
            Text = message.Text
        };
    }
        
    public static MessageResponse MapResponse(MessageEntity message, ReferenceMessage referenceMessage)
    {
        return new MessageResponse
        {
            Id = message.Id,
            ChatUserId = message.ChatUserId,
            DateCreated = message.DateCreated,
            DateDeleted = message.DateDeleted,
            DateUpdated = message.DateUpdated,
            Text = message.Text,
            ReferenceMessage = referenceMessage
        };
    }

    public static MessageResponseWithFiles MapResponseWithFiles(MessageEntity message, ReferenceMessage referenceMessage, List<Uri>? files)
    {
        return new MessageResponseWithFiles
        {
            Id = message.Id,
            ChatUserId = message.ChatUserId,
            DateCreated = message.DateCreated,
            DateDeleted = message.DateDeleted,
            DateUpdated = message.DateUpdated,
            Text = message.Text,
            ReferenceMessage = referenceMessage,
            Files = files
        };
    }
        
    public static MessageResponseWithFiles MapResponseWithFiles(MessageEntity message, List<Uri>? files)
    {
        return new MessageResponseWithFiles
        {
            Id = message.Id,
            ChatUserId = message.ChatUserId,
            DateCreated = message.DateCreated,
            DateDeleted = message.DateDeleted,
            DateUpdated = message.DateUpdated,
            Text = message.Text,
            Files = files
        };
    }

    public static LastMessageInChat MapLastMessageInChat(MessageEntity message)
    {
        return new LastMessageInChat
        {
            Id = message.Id,
            Text = message.Text,
            DateCreated = message.DateCreated,
            ChatUserId = message.ChatUserId
        };
    }
}