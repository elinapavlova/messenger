﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.User;

namespace Messenger.Core.Contracts.Services;

public interface IUserService
{
    Task<UserCreateResponse?> Add(UserCreateRequest request);
    Task<UserResponse?> GetByPhone(string phone);
    Task<UserResponse?> GetById(Guid id);
    Task<UserResponse?> Update(UserUpdateRequest request);
    Task<UserResponse?> Remove(Guid id);
}