﻿#nullable enable
using System;

namespace Messenger.Core.Dtos;

public class UserChatResponse
{
    public Guid ChatId { get; set; }
    public string? ChatName { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime DateComeIn { get; set; }
    public bool IsGroup { get; set; }
    public LastMessageInChat? LastMessage { get; set; }
}

public class LastMessageInChat
{
    public Guid Id { get; set; }
    public string? Text { get; set; }
    public DateTime DateCreated { get; set; }
    public Guid ChatUserId { get; set; }
}