﻿using System;
using System.Collections.Generic;
using Messenger.Core.Dtos.ChatUser;

namespace Messenger.Core.Dtos.Chat;

public class ChatResponse
{
    public Guid Id { get; set; }
    public List<ChatUserResponse> ChatUsers { get; set; }
    public DateTime DateCreated { get; set; }
    public bool IsGroup { get; set; }
    public string Name { get; set; }
}