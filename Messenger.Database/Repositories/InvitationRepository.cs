﻿#nullable enable
using System;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Database.Context;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Messenger.Database.Repositories;

public class InvitationRepository : BaseRepository<InvitationEntity, Guid>, IInvitationRepository
{
    public InvitationRepository(AppDbContext context) : base(context)
    {
    }

    public async Task<InvitationEntity?> Add(InvitationEntity invitation)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            if (IsInvitationValid(invitation) is false)
            {
                return null;
            }
            if (invitation.DateCreated == DateTime.MinValue)
            {
                invitation.DateCreated = DateTime.UtcNow;
            }
            var entry = await dbSet.AddAsync(invitation);
            return entry.Entity;
        });
    }

    public async Task<InvitationEntity?> GetById(Guid id)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var invitation = await dbSet.FirstOrDefaultAsync(i => i.Id == id);
            return IsInvitationValid(invitation) is false 
                ? null 
                : invitation;
        });
    }

    public async Task<InvitationEntity?> Update(InvitationEntity updatedInvitation)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var invitation = await dbSet.FirstOrDefaultAsync(i => i.Id == updatedInvitation.Id);
            if (invitation is null || IsInvitationValid(updatedInvitation) is false)
            {
                return null;
            }

            invitation.EntriesCount = updatedInvitation.EntriesCount;
            invitation.DateExpired = updatedInvitation.DateExpired;
            invitation.EntriesCountMax = updatedInvitation.EntriesCountMax;

            var entry = dbSet.Update(invitation);
            return entry.Entity;
        });
    }

    public async Task<int> CountActualInvitationsForChat(Guid chatId)
    {
        return await ExecuteWithResult(async dbSet =>
        {
            var invitations = await dbSet.Where(i => i.ChatId == chatId && i.EntriesCount < i.EntriesCountMax).ToListAsync();
            return invitations
                .Count(i => i.DateExpired is null || i.DateExpired is not null && i.DateExpired < DateTime.UtcNow);
        });
    }
        
    private static bool IsInvitationValid(InvitationEntity? invitation)
    {
        return (invitation is null || 
                invitation.DateExpired is not null && invitation.DateExpired >= DateTime.UtcNow || 
                invitation.EntriesCountMax is not null && invitation.EntriesCount == invitation.EntriesCountMax) is false;
    }
}