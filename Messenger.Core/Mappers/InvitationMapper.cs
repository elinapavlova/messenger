﻿using System;
using Messenger.Common.Helpers;
using Messenger.Core.Dtos.Invitation;
using Messenger.Database.Entities;
using Messenger.Database.Helpers;

namespace Messenger.Core.Mappers;

public static class InvitationMapper
{
    public static InvitationCreateResponse MapResponseCreated(Uri basePath, Guid invitationId)
    {
        return new InvitationCreateResponse
        {
            Url = InvitationHelper.CreateInvitationUrl(basePath, invitationId)
        };
    }

    public static InvitationEntity Map(InvitationResponse invitation)
    {
        return new InvitationEntity
        {
            Id = invitation.Id, ChatUserId = invitation.ChatUserId, DateExpired = invitation.DateExpired,
            EntriesCountMax = invitation.EntriesCountMax, EntriesCount = invitation.EntriesCount,
            DateCreated = invitation.DateCreated, ChatId = invitation.ChatId
        };
    }

    public static InvitationResponse MapResponse(InvitationEntity invitation)
    {
        return new InvitationResponse
        {
            Id = invitation.Id, ChatUserId = invitation.ChatUserId, DateCreated = invitation.DateCreated,
            DateExpired = invitation.DateExpired, EntriesCountMax = invitation.EntriesCountMax,
            EntriesCount = invitation.EntriesCount, ChatId = invitation.ChatId
        };
    }
}