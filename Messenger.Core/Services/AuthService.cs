﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Common;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.User;
using Messenger.Core.Logger;
using Messenger.Core.Mappers;
using Messenger.Database.Helpers;

namespace Messenger.Core.Services;

public class AuthService : IAuthService
{
    private readonly ITokenService _tokenService;
    private readonly IUserService _userService;
    private readonly ILogger _logger;

    public AuthService(ITokenService tokenService, IUserService userService,  ILogger logger)
    {
        _tokenService = tokenService;
        _userService = userService;
        _logger = logger;
    }

    public async Task<UserCreateResponse?> Register(UserCreateRequest request)
    {
        var result = await _userService.Add(request);
        if (result == null)
        {
            return null;
        }

        request.User = UserMapper.Map(result);
        result.AccessToken = await CreateAccessToken(UserMapper.MapTokenInfo(request.User));
            
        return result;
    }

    public async Task<UserLoginResponse?> Login(string phone)
    {
        var user = await _userService.GetByPhone(phone);
        if (user == null)
        {
            return null;
        }
            
        var token = await CreateAccessToken(UserMapper.MapTokenInfo(user));
        return AuthMapper.Map(user, token);
    }

    public async Task<AccessToken?> RefreshToken(string refreshToken)
    {
        var token = await _tokenService.GetByRefreshToken(refreshToken);
        if (token == null || TokenHelper.IsExpired(token.Expiration))
        {
            _logger.Error("Failed refresh token. Refresh token {token} is not valid", refreshToken);
            return null;
        }

        var user = await GetUserById(token.UserId);
        if (user is null)
        {
            _logger.Error($"Failed refresh token. User {token.UserId} doesn't exist");
            return null;
        }
        token.DateUsed = DateTime.UtcNow;
        await _tokenService.Update(token);
        var accessToken = await _tokenService.CreateAccessToken(UserMapper.MapTokenInfo(user));
        return accessToken;
    }

    private async Task<AccessToken> CreateAccessToken(UserTokenInfo user)
    {
        var result = await _tokenService.CreateAccessToken(user);
        return result;
    }

    private async Task<UserResponse?> GetUserById(Guid userId)
    {
        return await _userService.GetById(userId);
    }
}