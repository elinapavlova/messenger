﻿using System;
using System.Threading.Tasks;
using Messenger.Api.Dtos;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.Message;
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Messenger.Api.Controllers;

[ApiVersion("1.0")]
[Authorize]
[ApiController]
[Route("/api/v{version:apiVersion}/[controller]")]
public class MessagesController : Controller
{
    private readonly IMessageService _messageService;
    private readonly IChatUserService _chatUserService;
    private readonly IMessageFileService _messageFileService;

    public MessagesController
    (
        IMessageService messageService, 
        IChatUserService chatUserService,
        IMessageFileService messageFileService
    )
    {
        _messageService = messageService;
        _chatUserService = chatUserService;
        _messageFileService = messageFileService;
    }
        
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<MessageResponse>> Create(MessageCreateRequestDto dto)
    {
        var chatUser = await _chatUserService.GetById(dto.ChatUserId);
        if (chatUser is null)
        {
            return NotFound("User isn't in the chat");
        }

        if (dto.ReferenceMessageId is not null)
        {
            var referenceMessage = await _messageFileService.GetMessageByIdWithFiles((Guid) dto.ReferenceMessageId);
            if (referenceMessage is null)
            {
                return NotFound("Not found reference message");
            }
        }
            
        var message = await _messageFileService.Add(new MessageCreateRequest(new MessageEntity
        {
            ChatUserId = dto.ChatUserId, Text = dto.Text, ReferenceMessageId = dto.ReferenceMessageId
        }));
        if (message is null)
        {
            return BadRequest("Incorrect message text");
        }

        return Ok(message);
    }

    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<MessageResponse>> Update(MessageUpdateRequestDto dto)
    {
        var message = await _messageFileService.GetMessageByIdWithFiles(dto.Id);
        if (message is null)
        {
            return NotFound("Message doesn't exist");
        }
            
        var result = await _messageService.Update(new MessageUpdateRequest(new MessageEntity 
        {
            Id = dto.Id, ChatUserId = dto.ChatUserId, Text = dto.Text
        }));
        if (result is null)
        {
            return BadRequest("Incorrect message text");
        }

        return Ok(result);
    }
        
    [HttpGet("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<MessageResponseWithFiles>> GetById(Guid id)
    {
        var message = await _messageFileService.GetMessageByIdWithFiles(id);
        if (message is null)
        {
            return NotFound("Message doesn't exist");
        }

        return Ok(message);
    }
        
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Remove(Guid id)
    {
        var message = await _messageFileService.RemoveMessageByIdWithFiles(id);
        if (message is null)
        {
            return NotFound("Message doesn't exist");
        }
            
        return NoContent();
    }
}