﻿using System;
using System.Collections.Generic;
using Messenger.Core.Dtos.ChatUser;

namespace Messenger.Core.Dtos.Chat;

public class ChatCreateResponse
{
    public Guid Id { get; set; }
    public List<ChatUserResponse> ChatUsers { get; set; }
    public DateTime DateCreated { get; set; }
}