﻿#nullable enable
using System.Collections.Generic;
using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.GroupChat;

public class GroupChatCreateRequest
{
    public readonly List<UserEntity>? Users;
    public readonly ChatEntity GroupChat;
        
    public GroupChatCreateRequest(ChatEntity groupChat, List<UserEntity>? items)
    {
        GroupChat = groupChat;
        Users = items;
    }
}