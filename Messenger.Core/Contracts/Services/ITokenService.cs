﻿using System;
using System.Threading.Tasks;
using Messenger.Common;
using Messenger.Database.Entities;

namespace Messenger.Core.Contracts.Services;

public interface ITokenService
{
    Task<AccessToken> CreateAccessToken(UserTokenInfo user);
    Task<RefreshTokenEntity> Update(RefreshTokenEntity refreshToken);
    Task<RefreshTokenEntity> GetByRefreshToken(string refreshToken);
    UserTokenInfo GetUserInfoByToken(string token);
    Task ExitUserSessions(Guid userId);
}