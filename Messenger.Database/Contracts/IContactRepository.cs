﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Database.Entities;

namespace Messenger.Database.Contracts;

public interface IContactRepository
{
    Task<ContactEntity?> Add(ContactEntity contact);
    Task<ContactEntity?> Update(ContactEntity updatedContact);
    Task<ContactEntity?> GetById(Guid id);
    Task<List<ContactEntity>?> GetByUserId(Guid userId, Filter filter);
    Task<ContactEntity?> GetByUserIdContactUserId(Guid userId, Guid contactUserId);
    Task<ContactEntity?> Remove(Guid id);
}