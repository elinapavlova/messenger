﻿using System;

namespace Messenger.Api.Dtos;

public record GroupChatUpdateRequestDto
{
    public Guid Id { get; set; }
    public string NewName { get; set; }
}