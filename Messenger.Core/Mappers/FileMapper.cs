﻿using System;
using System.Collections.Generic;
using System.Linq;
using Messenger.Core.Dtos.File;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class FileMapper
{
    public static FileResponse MapResponse(FileEntity file)
    {
        return new FileResponse
        {
            Id = file.Id,
            Name = file.Name,
            Path = file.Path,
            Extension = file.Extension,
            DateCreated = file.DateCreated
        };
    }
        
    public static FileResponse MapResponse(FileEntity file, Uri url)
    {
        return new FileResponse
        {
            Id = file.Id,
            Name = file.Name,
            Path = file.Path,
            Extension = file.Extension,
            DateCreated = file.DateCreated,
            Url = url
        };
    }
        
    public static List<FileResponse> MapResponse(IEnumerable<FileEntity> files)
    {
        return files.Select(MapResponse).ToList();
    }

    public static FileEntity Map(FileUpload file, Guid messageId, Guid chatId)
    {
        return new FileEntity
        {
            Name = file.Name,
            Path = file.Path,
            Extension = file.Extension,
            MessageId = messageId,
            ChatId = chatId
        };
    }
}