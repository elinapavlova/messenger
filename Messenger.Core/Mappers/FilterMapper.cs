﻿using Messenger.Common.Filter;
using Messenger.Core.Options;

namespace Messenger.Core.Mappers;

public static class FilterMapper
{
    public static Filter Map(FilterDto filterDto, PagingOptions pagingOptions)
    {
        return new Filter
        {
            Paging = new Paging
            {
                PageNumber = CheckPageNumber(filterDto.PageNumber, pagingOptions.DefaultPageNumber),
                PageSize = CheckPageSize(filterDto.PageSize, pagingOptions.DefaultPageSize)
            }
        };
    }

    private static int CheckPageSize(int? pageSize, int defaultPageSize)
    {
        return pageSize is null or < 1 
            ? defaultPageSize 
            : (int) pageSize;
    }

    private static int CheckPageNumber(int? pageNumber, int defaultPageNumber)
    {
        return pageNumber is null or < 1 
            ? defaultPageNumber 
            : (int) pageNumber;
    }
}