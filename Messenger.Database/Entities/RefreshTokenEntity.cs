﻿using System;

namespace Messenger.Database.Entities;

public class RefreshTokenEntity : BaseEntity<Guid>
{
    public string Token { get; set; }
    public long Expiration { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime DateExpired { get; set; }
    public DateTime? DateUsed { get; set; }
    public Guid UserId { get; set; }
        
    public UserEntity User { get; set; }
}