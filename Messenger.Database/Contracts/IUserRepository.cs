﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Database.Entities;

namespace Messenger.Database.Contracts;

public interface IUserRepository
{
    Task<UserEntity?> Add(UserEntity user);
    Task<UserEntity?> GetByPhone(string phone);
    Task<UserEntity?> GetById(Guid id);
    Task<UserEntity?> Update(UserEntity userUpdated);
    Task<UserEntity?> Remove(Guid id);
}