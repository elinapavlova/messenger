﻿namespace Messenger.Common;

public class UserTokenInfo
{
    public Guid Id { get; set; }
    public string Phone { get; set; }
}