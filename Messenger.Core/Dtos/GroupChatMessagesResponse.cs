﻿using System;
using System.Collections.Generic;
using Messenger.Core.Dtos.GroupChatUser;
using Messenger.Core.Dtos.Message;

namespace Messenger.Core.Dtos;

public class GroupChatMessagesResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<GroupChatUserResponse> ChatUsers { get; set; }
    public List<MessageResponseWithFiles> Messages { get; set; }
    public DateTime DateCreated { get; set; }
}