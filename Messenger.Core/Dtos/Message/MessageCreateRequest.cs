﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.Message;

public class MessageCreateRequest
{
    public readonly MessageEntity Message;

    public MessageCreateRequest(MessageEntity message)
    {
        Message = message;
    }
}