﻿namespace Messenger.Common.Helpers;

public static class InvitationHelper
{
    public static Uri CreateInvitationUrl(Uri basePath, Guid invitationId)
    {
        var path = Path.Combine("invitations",invitationId.ToString());
        var url = new Uri(basePath, path);
        return url;
    }
}