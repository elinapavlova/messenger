﻿using System;
using System.Threading.Tasks;
using Messenger.Api.Dtos;
using Messenger.Core.Contracts;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.Contact;
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Messenger.Api.Controllers;

[ApiVersion("1.0")]
[Authorize]
[ApiController]
[Route("/api/v{version:apiVersion}/[controller]")]
public class ContactsController : Controller
{
    private readonly IContactService _contactService;

    public ContactsController(IContactService contactService)
    {
        _contactService = contactService;
    }
        
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<ContactCreateResponse>> Create(ContactCreateRequestDto dto)
    {
        var contact = await _contactService.GetByUserIdContactUserId(dto.UserId, dto.ContactUserId);
        if (contact is not null)
        {
            return BadRequest("Contact is already exist");
        }
        var result = await _contactService.Add(new ContactCreateRequest(new ContactEntity
        {
            ContactName = dto.ContactName, UserId = dto.UserId, ContactUserId = dto.ContactUserId
        }));
        if (result is null)
        {
            return NotFound("Some users aren't exist or contact name is not valid");
        }

        return Ok(result);
    }

    [HttpPut("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<ContactResponse>> Update(Guid id, ContactUpdateRequestDto dto)
    {
        var contact = await _contactService.GetById(id);
        if (contact is null)
        {
            return NotFound("Contact doesn't exist");
        }
        var result = await _contactService.Update(new ContactUpdateRequest(new ContactEntity
        {
            Id = id, ContactName = dto.NewContactName
        }));
        if (result is null)
        {
            return BadRequest("Failed update contact");
        }

        return Ok(result);
    }
        
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Remove(Guid id)
    {
        var contact = await _contactService.Remove(id);
        if (contact is null)
        {
            return NotFound("Contact doesn't exist");
        }

        return NoContent();
    }
}