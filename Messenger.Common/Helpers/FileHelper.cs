﻿#nullable enable
namespace Messenger.Common.Helpers;

public static class FileHelper
{
    public static string? CreateAbsolutePath(string? directory, string basePath, Guid chatId, string fileName)
    {
        var extension = Path.GetExtension(fileName).ToLower();
        string? absolutePath = null;
        if (string.IsNullOrEmpty(directory))
        {
            return null;
        }

        switch (extension)
        {
            case ".mp4" :
                absolutePath = Path.Combine(directory, basePath, chatId.ToString(), "video", Guid.NewGuid() + extension);
                break;
            case ".png" :
            case ".jpg" :
            case ".jpeg":
                absolutePath = Path.Combine(directory, basePath, chatId.ToString(), "images", Guid.NewGuid() + extension);
                break;
        }
        return absolutePath;
    }

    private static bool? IsDirectoryExist(string? directoryName)
    {
        if (directoryName is null)
        {
            return null;
        }
        return Directory.Exists(directoryName);
    }

    public static bool IsFileNameValid(string fileName)
    {
        return !string.IsNullOrEmpty(fileName) && !string.IsNullOrWhiteSpace(fileName) &&
               fileName.Length is > 0 and <= 50;
    }
        
    public static bool IsFileExist(string path)
    {
        return File.Exists(path);
    }

    private static void CreateDirectory(string directoryName)
    {
        Directory.CreateDirectory(directoryName);
    }
        
    public static void CheckDirectory(string? directoryName)
    {
        var isDirectoryExist = IsDirectoryExist(directoryName);
        switch (isDirectoryExist)
        {
            case null:
                throw new Exception($"Directory name {directoryName} is not valid");
            case false:
                CreateDirectory(directoryName);
                break;
        }
    }
        
    public static void RevertSavingFiles(IEnumerable<string> filePaths)
    {
        foreach (var path in filePaths.Where(File.Exists))
        {
            File.Delete(path);
        }
    }
}