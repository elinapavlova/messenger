﻿using System;

namespace Messenger.Core.Dtos.File;

public class FileResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Path { get; set; }
    public string Extension { get; set; }
    public DateTime DateCreated { get; set; }
    public Uri Url { get; set; }
}