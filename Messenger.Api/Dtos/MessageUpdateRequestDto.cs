﻿using System;

namespace Messenger.Api.Dtos;

public record MessageUpdateRequestDto
{
    public Guid Id { get; set; }
    public string Text { get; set; }
    public Guid ChatUserId { get; set; }
}