﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Contracts;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.User;
using Messenger.Core.Mappers;

namespace Messenger.Core.Services;

public class UserChatService : IUserChatService
{
    private readonly IChatService _chatService;
    private readonly IChatUserService _chatUserService;
    private readonly IGroupChatService _groupChatService;
    private readonly IMessageService _messageService;

    public UserChatService
    (
        IChatService chatService, 
        IChatUserService chatUserService, 
        IMessageService messageService, 
        IGroupChatService groupChatService
    )
    {
        _chatService = chatService;
        _chatUserService = chatUserService;
        _messageService = messageService;
        _groupChatService = groupChatService;
    }
    
    public async Task<List<UserChatResponse>?> GetChats(Guid userId, FilterDto filter)
    {
        var items = new List<UserChatResponse>();

        var chatsByUserId = await _chatService.GetByUserId(userId, filter);
        if (chatsByUserId == null)
        {
            return items;
        }
        foreach (var chat in chatsByUserId)
        {
            var chatUser = await _chatUserService.GetByChatIdUserId(chat.Id, userId);
            if (chatUser is null)
            {
                continue;
            }
            var lastMessage = await _messageService.GetLastByChatId(chat.Id);
                
            items.Add(UserMapper.MapUserChatResponse(chat, chatUser, lastMessage));
        }

        return items.OrderByDescending(c => c.LastMessage?.DateCreated).ToList();
    }
    
    public async Task<UserResponse?> LeaveChat(UserLeaveChatRequest request)
    {
        var chat = await _groupChatService.GetById(request.Chat.Id);
        var chatUser = chat?.ChatUsers.FirstOrDefault(cu => cu.UserId == request.User.Id);
        if (chatUser is null)
        {
            return null;
        }

        var result = await _chatUserService.ComeOut(chatUser.Id);
        if (result is null)
        {
            return null;
        }

        return new UserResponse
        {
            Id = chatUser.UserId
        };
    }
}