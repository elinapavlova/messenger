﻿using Messenger.Core.Contracts.Provider;
using Messenger.Core.Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Messenger.Core.Providers;

public class LoggerProvider : IDependencyProvider
{
    public void Register(IServiceCollection services)
    {
        services.AddScoped<ILogger, Logger.Logger>();
    }
}