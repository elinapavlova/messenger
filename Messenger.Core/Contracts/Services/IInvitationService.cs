﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Core.Dtos.ChatUser;
using Messenger.Core.Dtos.Invitation;

namespace Messenger.Core.Contracts;

public interface IInvitationService
{
    Task<InvitationCreateResponse?> Add(InvitationCreateRequest? request);
    Task<InvitationResponse?> GetById(Guid id);
    Task<ChatUserResponse?> EnterChatByInvitation(ChatEntryByInvitationRequest? request);
}