﻿using System;
using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.Invitation;

public class InvitationCreateRequest
{
    public readonly ChatUserEntity ChatUser;
    public readonly ChatEntity Chat;
    public readonly DateTime? DateExpired;
    public readonly int? EntriesCountMax;

    public InvitationCreateRequest(ChatUserEntity chatUser, ChatEntity chat, DateTime? dateExpired = null, int? entriesCountMax = null)
    {
        ChatUser = chatUser;
        Chat = chat;
        DateExpired = dateExpired;
        EntriesCountMax = entriesCountMax;
    }
}