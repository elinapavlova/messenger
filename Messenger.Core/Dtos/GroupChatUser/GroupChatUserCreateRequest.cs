﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.GroupChatUser;

public class GroupChatUserCreateRequest
{
    public ChatEntity GroupChat { get; set; }
    public UserEntity User { get; set; }
}