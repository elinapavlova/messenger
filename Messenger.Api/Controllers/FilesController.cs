﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.File;
using Messenger.Core.Dtos.Message;
using Messenger.Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Messenger.Api.Controllers;

[ApiVersion("1.0")]
[Authorize]
[ApiController]
[Route("/api/v{version:apiVersion}/[controller]")]
public class FilesController : Controller
{
    private readonly IFileService _fileService;
    private readonly IChatUserService _chatUserService;
    private readonly IMessageService _messageService;

    public FilesController
    (
        IFileService fileService, 
        IChatUserService chatUserService,
        IMessageService messageService
    )
    {
        _fileService = fileService;
        _chatUserService = chatUserService;
        _messageService = messageService;
    }
        
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<FilesUploadResponse>> Upload(Guid chatUserId, Guid? referenceMessageId, 
        IFormFileCollection files, [FromForm] string? text)
    {
        if (files.Count is <= 0 or > 10)
        {
            return BadRequest("Incorrect files count");
        }
        var chatUser = await _chatUserService.GetById(chatUserId);
        if (chatUser is null)
        {
            return NotFound("Not found user in chat");
        }

        var referenceMessage = new MessageResponse();
        if (referenceMessageId is not null)
        {
            referenceMessage = await _messageService.GetById((Guid)referenceMessageId);
        }
            
        var result = await _fileService.Upload(new FilesUploadRequest(new ChatUserEntity 
            {
                Id = chatUser.Id
            }, 
            files,
            text, 
            referenceMessage is null ? null : new MessageEntity
            {
                Id = referenceMessage.Id
            }));
                
        if (result is null)
        {
            return BadRequest("Failed file upload");
        }

        return Ok(result);
    }
}