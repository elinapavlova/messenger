﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.Contact;
using Messenger.Core.Mappers;
using Messenger.Core.Options;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;

namespace Messenger.Core.Services;

public class ContactService : IContactService
{
    private readonly IContactRepository _contactRepository;
    private readonly IUserRepository _userRepository;
    private readonly IChatRepository _chatRepository;
    private readonly IChatUserRepository _chatUserRepository;
    private readonly PagingOptions _pagingOptions;

    public ContactService
    (
        IContactRepository contactRepository, 
        IUserRepository userRepository,
        IChatRepository chatRepository,
        IChatUserRepository chatUserRepository,
        FilterOptions filterOptions
    )
    {
        _contactRepository = contactRepository;
        _userRepository = userRepository;
        _chatRepository = chatRepository;
        _chatUserRepository = chatUserRepository;
        _pagingOptions = filterOptions.Filters["Contacts"] ?? filterOptions.Filters["Default"];
    }

    public async Task<ContactCreateResponse?> Add(ContactCreateRequest request)
    {
        if (await IsAddRequestValid(request) is false)
        {
            return null;
        }
        var contact = await CreateContact(request.Contact);
        if (contact == null)
        {
            return null;
        }
        var chat = await GetChatByUsersIds(request.Contact.UserId, request.Contact.ContactUserId);
        if (chat != null)
        {
            // Переименовать существующий чат именем контакта
            await RenameChatUser(chat.Id, request.Contact.UserId, contact.ContactName);
        }
        return ContactMapper.MapCreatedResponse(contact);
    }

    public async Task<ContactResponse?> Update(ContactUpdateRequest dto)
    {
        var contact = await _contactRepository.Update(dto.Contact);
        if (contact == null)
        {
            return null;
        }
            
        var chat = await GetChatByUsersIds(contact.UserId, contact.ContactUserId);
        if (chat == null)
        {
            return ContactMapper.MapResponse(contact);
        }
        var renamedChatUser = await RenameChatUser(chat.Id, contact.UserId, contact.ContactName);
        return renamedChatUser is null 
            ? null
            : ContactMapper.MapResponse(contact);
    }

    public async Task<ContactResponse?> Remove(Guid id)
    {
        var contact = await _contactRepository.Remove(id);
        if (contact == null)
        {
            return null;
        }
            
        var chat = await GetChatByUsersIds(contact.UserId, contact.ContactUserId);
        if (chat == null)
        {
            return ContactMapper.MapResponse(contact);
        }

        var chatUser = await GetChatUserByChatIdUserId(chat.Id, contact.UserId);
        var userContact = await GetUserById(contact.ContactUserId);
        if (chatUser == null || userContact == null)
        {
            return ContactMapper.MapResponse(contact);
        }
        // Переименовать чат с пользователем его ником в мессенджере
        await RenameChatUser(chat.Id, userContact.Id, userContact.UserName);
        return ContactMapper.MapResponse(contact);
    }

    public async Task<ContactResponse?> GetById(Guid id)
    {
        var contact = await _contactRepository.GetById(id);
        return contact is null 
            ? null 
            : ContactMapper.MapResponse(contact);
    }

    public async Task<List<ContactResponse>?> GetByUserId(Guid userId, FilterDto filter)
    {
        var contacts = await _contactRepository.GetByUserId(userId, FilterMapper.Map(filter, _pagingOptions));
        return contacts?.Select(ContactMapper.MapResponse).ToList();
    }

    public async Task<ContactResponse?> GetByUserIdContactUserId(Guid userId, Guid contactUserId)
    {
        var contact = await _contactRepository.GetByUserIdContactUserId(userId, contactUserId);
        return contact is null 
            ? null 
            : ContactMapper.MapResponse(contact);
    }

    private async Task<bool> IsAddRequestValid(ContactCreateRequest? request)
    {
        if (request is null)
        {
            return false;
        }
        var users = await GetUsersByIds(request.Contact.UserId, request.Contact.ContactUserId);
        if (users is null)
        {
            return false;
        }
        var isContactExist = await GetByUserIdContactUserId(request.Contact.UserId, request.Contact.ContactUserId);
        return isContactExist is null;
    }
        
    private async Task<List<UserEntity>?> GetUsersByIds(Guid userId, Guid contactUserId)
    {
        var users = new List<UserEntity>();
        var user = await GetUserById(userId);
        if (user is null)
        {
            return null;
        }
        users.Add(user);
        var userContact = await GetUserById(contactUserId);
        if (userContact is null)
        {
            return null;
        }
        users.Add(userContact);
        return users;
    }
        
    private async Task<UserEntity?> GetUserById(Guid userId)
    {
        return await _userRepository.GetById(userId);
    }

    private async Task<ContactEntity?> CreateContact(ContactEntity contact)
    {
        return await _contactRepository.Add(contact);
    }
        
    private async Task<ChatEntity?> GetChatByUsersIds(Guid userId, Guid contactUserId)
    {
        return await _chatRepository.GetByUserIds(userId, contactUserId);
    }

    private async Task<ChatUserEntity?> RenameChatUser(Guid chatId, Guid userId, string contactName)
    {
        try
        {
            var chatUser = await GetChatUserByChatIdUserId(chatId, userId);
            if (chatUser is null)
            {
                throw new Exception($"Failed rename user chat. UserId {userId} ChatId {chatId}. ChatUser not found");
            }

            chatUser.Name = contactName;
            var updatedChatUser = await _chatUserRepository.Update(chatUser);
            if (updatedChatUser is null)
            {
                throw new Exception($"Failed rename chat ChatId {chatId} UserId {userId} ChatUserId {chatUser.Id} " +
                                    $"ChatUser new name {contactName}");
            }
            return updatedChatUser;
        }
        catch(Exception exception)
        {
            Console.WriteLine(exception.Message);
            return null;
        }
    }

    private async Task<ChatUserEntity?> GetChatUserByChatIdUserId(Guid chatId, Guid userId)
    {
        return await _chatUserRepository.GetByChatIdUserId(chatId, userId);
    }
}