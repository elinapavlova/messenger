﻿using System;
using System.Text;
using Messenger.Common.Helpers;
using Messenger.Core.Contracts.Provider;
using Messenger.Core.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Messenger.Core.Providers;

public class AuthenticationProvider : IDependencyProviderWithConfiguration
{
    public void Register(IServiceCollection services, IConfiguration configuration)
    {
        
        var appOptions = configuration.GetOptions<AppOptions>("App");
        var securityKeyBytes = Encoding.ASCII.GetBytes(appOptions.Secret);
        
        services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(securityKeyBytes),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    RequireExpirationTime = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
            });
    }
}