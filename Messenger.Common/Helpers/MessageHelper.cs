﻿#nullable enable
namespace Messenger.Common.Helpers;

public static class MessageHelper
{
    public static bool IsTextValid(string? text)
    {
        return !string.IsNullOrEmpty(text) && 
               !string.IsNullOrWhiteSpace(text) && 
               text.Length is > 0 and <= 1000;
    }
}