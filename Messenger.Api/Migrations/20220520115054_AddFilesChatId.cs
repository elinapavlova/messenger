﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Messenger.Api.Migrations
{
    public partial class AddFilesChatId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ChatId",
                table: "Files",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Files_ChatId",
                table: "Files",
                column: "ChatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Chats_ChatId",
                table: "Files",
                column: "ChatId",
                principalTable: "Chats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_Chats_ChatId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_ChatId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "ChatId",
                table: "Files");
        }
    }
}
