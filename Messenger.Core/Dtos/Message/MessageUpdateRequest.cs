﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.Message;

public class MessageUpdateRequest
{
    public readonly MessageEntity Message;

    public MessageUpdateRequest(MessageEntity message)
    {
        Message = message;
    }
}