﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Messenger.Common;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Logger;
using Messenger.Core.Mappers;
using Messenger.Core.Options;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;
using Microsoft.IdentityModel.Tokens;

namespace Messenger.Core.Services;

public class TokenService : ITokenService
{
    private readonly string _secretKey;
    private readonly IRefreshTokenRepository _tokenRepository;
    private readonly ILogger _logger;

    public TokenService(AppOptions options, IRefreshTokenRepository tokenRepository, ILogger logger)
    {
        _secretKey = options.Secret;
        _tokenRepository = tokenRepository;
        _logger = logger;
    }
        
    public async Task<AccessToken> CreateAccessToken(UserTokenInfo user)
    {
        var handler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_secretKey);
        var accessTokenExpiration = DateTime.UtcNow.AddMinutes(120);

        var securityToken = new JwtSecurityToken
        (
            claims : GetClaims(user),
            expires : accessTokenExpiration,
            notBefore : DateTime.UtcNow,
            signingCredentials : 
            new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        );
            
        var accessToken = handler.WriteToken(securityToken);
        var refreshToken = await CreateRefreshToken(user.Id);
        return TokenMapper.Map(accessToken, refreshToken, accessTokenExpiration.Ticks);
    }

    public UserTokenInfo GetUserInfoByToken(string token)
    {
        var handler = new JwtSecurityTokenHandler();
        var securityToken = handler.ReadJwtToken(token);
        var claims = securityToken.Claims.ToList();
        
        return new UserTokenInfo
        {
            Id = new Guid(claims.First(x => x.Type == ClaimTypes.Name).Value),
            Phone = claims.First(x => x.Type == ClaimTypes.MobilePhone).Value
        };
    }

    private static IEnumerable<Claim> GetClaims(UserTokenInfo user)
    {
        var claims = new List<Claim>
        {
            new (ClaimTypes.Name, user.Id.ToString()),
            new (ClaimTypes.MobilePhone, user.Phone)
        };

        return claims;
    }

    private async Task<string> CreateRefreshToken(Guid userId)
    {
        var canUserAddToken = await CanUserAddToken(userId);
        if (canUserAddToken is false)
        {
            await ExitUserSessions(userId);
            _logger.Error($"user {userId} have max count of active sessions. All sessions were finished.");
        }
            
        var expiration = DateTime.UtcNow.AddDays(120).Ticks;
        var refreshToken = Convert.ToBase64String(Encoding.ASCII.GetBytes(userId.ToString()));

        var token = await _tokenRepository.Add(new RefreshTokenEntity
        {
            Token = refreshToken, 
            Expiration = expiration, 
            DateCreated = DateTime.UtcNow,
            DateExpired = DateTime.UtcNow.AddDays(120),
            UserId = userId
        });
        if (token != null)
        {
            return refreshToken;
        }
        _logger.Error($"Failed create refresh token\r\nData: token - {refreshToken}" +
                      $"expiration - {expiration} , userId - {userId}");
        return null;
    }

    // Ограничение на количество активных сеансов
    private async Task<bool> CanUserAddToken(Guid userId)
    {
        var tokens = await _tokenRepository.GetByUserId(userId);
        return tokens.Count < 10;
    }

    // Если у пользователя больше 10 активных сеансов - завершить все
    public async Task ExitUserSessions(Guid userId)
    {
        var tokens = await _tokenRepository.GetByUserId(userId);
        foreach (var token in tokens)
        {
            token.DateExpired = DateTime.UtcNow;
        }
        var updatedTokens = await _tokenRepository.UpdateList(tokens);

        if (updatedTokens.Count == tokens.Count)
        {
            return;
        }
        _logger.Error($"Error exiting user {userId} sessions:\r\n" + 
                      CreateErrorExitingSessionText(tokens.Select(x => x.Token),
                          updatedTokens.Select(x => x.Token).ToList()));
    }

    public async Task<RefreshTokenEntity> Update(RefreshTokenEntity refreshToken)
    {
        return await _tokenRepository.Update(refreshToken);
    }

    public async Task<RefreshTokenEntity> GetByRefreshToken(string refreshToken)
    {
        return await _tokenRepository.GetByRefreshToken(refreshToken);
    }
        
    private static string CreateErrorExitingSessionText(IEnumerable<string> allTokens, ICollection<string> exitingTokens)
    {
        var text = string.Empty;
        foreach (var token in allTokens)
        {
            if (exitingTokens.Contains(token))
            {
                text += $"Update refresh token {token}";
                continue;
            }

            text += $"Failed update refresh token {token}";
        }

        return text;
    }
}