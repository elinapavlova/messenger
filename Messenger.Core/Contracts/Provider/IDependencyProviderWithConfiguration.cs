﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Messenger.Core.Contracts.Provider;

public interface IDependencyProviderWithConfiguration
{
    void Register(IServiceCollection services, IConfiguration configuration);
}