﻿using System;
using System.Collections.Generic;
using Messenger.Core.Dtos.ChatUser;
using Messenger.Core.Dtos.Message;

namespace Messenger.Core.Dtos;

public class ChatMessagesResponse
{
    public Guid Id { get; set; }
    public List<ChatUserResponse> ChatUsers { get; set; }
    public List<MessageResponseWithFiles> Messages { get; set; }
    public DateTime DateCreated { get; set; }
}