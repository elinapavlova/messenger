﻿#nullable enable
using System.Collections.Generic;
using System.Linq;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.Chat;
using Messenger.Core.Dtos.ChatUser;
using Messenger.Core.Dtos.Message;
using Messenger.Database.Entities;

namespace Messenger.Core.Mappers;

public static class ChatMapper
{
    public static ChatCreateResponse MapCreatedResponse(ChatEntity chat)
    {
        var chatUsers = ChatUserMapper.MapResponse(chat.Id, chat.ChatUsers);
            
        return new ChatCreateResponse
        {
            Id = chat.Id, 
            ChatUsers = chatUsers, 
            DateCreated = chat.DateCreated
        };
    }

    public static ChatResponse MapResponse(ChatEntity chat)
    {
        var chatUsers = chat.ChatUsers is not null
            ? ChatUserMapper.MapResponse(chat.Id, chat.ChatUsers)
            : new List<ChatUserResponse>();

        return new ChatResponse
        {
            Id = chat.Id, 
            ChatUsers = chatUsers, 
            DateCreated = chat.DateCreated,
            IsGroup = chat.IsGroup,
            Name = chat.Name
        };
    }
        
    public static List<ChatResponse> MapResponse(IEnumerable<ChatEntity> chats)
    {
        return chats.Select(MapResponse).ToList();
    }

    public static ChatMessagesResponse MapChatMessagesResponse(ChatEntity chat, List<MessageResponseWithFiles>? messages)
    {
        var chatUsers = ChatUserMapper.MapResponse(chat.Id, chat.ChatUsers);

        return new ChatMessagesResponse
        {
            Id = chat.Id,
            ChatUsers = chatUsers, 
            DateCreated = chat.DateCreated, 
            Messages = messages
        };
    }
}