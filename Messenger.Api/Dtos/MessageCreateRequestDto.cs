﻿#nullable enable
using System;

namespace Messenger.Api.Dtos;

public record MessageCreateRequestDto
{
    public Guid ChatUserId { get; set; }
    public Guid? ReferenceMessageId { get; set; }
    public string? Text { get; set; }
}