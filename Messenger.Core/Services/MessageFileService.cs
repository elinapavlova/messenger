﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Common.Helpers;
using Messenger.Core.Contracts.Services;
using Messenger.Core.Dtos.File;
using Messenger.Core.Dtos.Message;
using Messenger.Core.Logger;
using Messenger.Core.Mappers;
using Messenger.Core.Options;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;

namespace Messenger.Core.Services;

public class MessageFileService : IMessageFileService
{
    private readonly IFileService _fileService;
    private readonly IMessageRepository _messageRepository;
    private readonly IChatUserService _chatUserService;
    private readonly ILogger _logger;
    private readonly PagingOptions _messagesPagingOptions;

    public MessageFileService
    (
        IFileService fileService, 
        IMessageRepository messageRepository,
        IChatUserService chatUserService,
        ILogger logger,
        FilterOptions options
    )
    {
        _fileService = fileService;
        _messageRepository = messageRepository;
        _chatUserService = chatUserService;
        _logger = logger;
        _messagesPagingOptions = options.Filters["Messages"] ?? options.Filters["Default"];
    }

    public async Task<MessageResponse?> RemoveMessageByIdWithFiles(Guid id)
    {
        var message = await _messageRepository.Remove(id);
        if (message is null)
        {
            return null;
        }

        await RemoveFilesByMessageId(message.Id);
        return MessageMapper.MapResponse(message);
    }

    private async Task RemoveFilesByMessageId(Guid messageId)
    {
        var files = await GetFilesByMessageId(messageId);
        if (files is null)
        {
            return;
        }

        foreach (var file in files)
        {
            var result = await _fileService.Remove(file.Id);
            if (result is null)
            {
                _logger.Error("Failed delete file Id {fileId} Path {filePath}", file.Id, file.Path);
            }
        }
    }

    public async Task<MessageResponseWithFiles?> GetMessageByIdWithFiles(Guid id)
    {
        var message = await _messageRepository.GetById(id);
        if (message is null || message.DateDeleted is not null)
        {
            return null;
        }
            
        var files = await GetFileUrlsByMessageId(message.Id);
        if (message.ReferenceMessageId is null)
        {
            return MessageMapper.MapResponseWithFiles(message, files);
        }
            
        var referenceMessage = await GetReferenceMessageById((Guid) message.ReferenceMessageId);
        return referenceMessage is null 
            ? MessageMapper.MapResponseWithFiles(message, files) 
            : MessageMapper.MapResponseWithFiles(message, referenceMessage, files);
    }

    public async Task<List<MessageResponseWithFiles>?> GetMessagesByChatIdWithFiles(Guid chatId, FilterDto filter)
    {
        var result = new List<MessageResponseWithFiles>();
        var messages = await _messageRepository.GetByChatId(chatId, FilterMapper.Map(filter, _messagesPagingOptions));
        if (messages is null)
        {
            return null;
        }

        foreach (var message in messages)
        {
            var fileUrls = await GetFileUrlsByMessageId(message.Id);
            if (message.ReferenceMessageId is null)
            {
                result.Add(MessageMapper.MapResponseWithFiles(message, fileUrls));
                continue;
            }
            var referenceMessage = await GetReferenceMessageById((Guid) message.ReferenceMessageId);
            if (referenceMessage is not null)
            {
                result.Add(MessageMapper.MapResponseWithFiles(message, referenceMessage, fileUrls));
            }
        }

        return result;
    }
    
    public async Task<MessageResponse?> Add(MessageCreateRequest request)
    {
        var chatUser = await _chatUserService.GetById(request.Message.ChatUserId);
        if (chatUser is null || (MessageHelper.IsTextValid(request.Message.Text?.Trim()) is false 
                                 && request.Message.ReferenceMessageId is null))
        {
            return null;
        }

        var message = await _messageRepository.Add(new MessageEntity
        {
            Text = request.Message.Text, 
            ChatUserId = chatUser.Id, 
            ChatId = chatUser.ChatId, 
            ReferenceMessageId = request.Message.ReferenceMessageId
        });
            
        if (request.Message.ReferenceMessageId is null)
        {
            return message is null ? null : MessageMapper.MapResponse(message);
        }

        var referenceMessage = await GetReferenceMessageById((Guid) request.Message.ReferenceMessageId);
        if (message is null || referenceMessage is null)
        {
            return null;
        }
        return MessageMapper.MapResponse(message, referenceMessage);
    }
    
    private async Task<ReferenceMessage?> GetReferenceMessageById(Guid id)
    {
        var referenceMessage = await _messageRepository.GetById(id);
        if (referenceMessage is null)
        {
            return null;
        }

        var files = await GetFileUrlsByMessageId(id);
        return MessageMapper.MapReferenceMessage(referenceMessage, files);
    }
    
    private async Task<List<Uri>?> GetFileUrlsByMessageId(Guid id)
    {
        var files = await GetFilesByMessageId(id);

        var urls = files?
            .Select(file => _fileService.CreateUrl(file.Path))
            .Where(url => url is not null)
            .ToList();

        return urls;
    }
    
    private async Task<List<FileResponse>?> GetFilesByMessageId(Guid id)
    {
        return await _fileService.GetByMessageId(id);
    }
}