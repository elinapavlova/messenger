﻿using System;
using System.Collections.Generic;

namespace Messenger.Database.Entities;

public class ChatUserEntity : BaseEntity<Guid>
{
    public Guid ChatId { get; set; }
    public Guid UserId { get; set; }
    public string Name { get; set; }
    public DateTime DateStarted { get; set; }
    public DateTime? DateComeOut { get; set; }
        
    public ChatEntity Chat { get; set; }
    public UserEntity User { get; set; }
    public List<MessageEntity> Messages { get; set; }
    public List<InvitationEntity> Invitations { get; set; }
}