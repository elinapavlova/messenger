﻿#nullable enable
using System;
using System.Threading.Tasks;
using Messenger.Core.Contracts.Services;
using Messenger.Database.Contracts;
using Messenger.Database.Entities;

namespace Messenger.Core.Services;

public class ChatUserContactNameService : IChatUserContactNameService
{
    private readonly IChatUserRepository _chatUserRepository;
    private readonly IContactRepository _contactRepository;
    private readonly IUserRepository _userRepository;

    public ChatUserContactNameService
    (
        IChatUserRepository chatUserRepository, 
        IContactRepository contactRepository, 
        IUserRepository userRepository
    )
    {
        _chatUserRepository = chatUserRepository;
        _contactRepository = contactRepository;
        _userRepository = userRepository;
    }

    public async Task<ChatUserEntity?> GetChatUserWithContactName(Guid chatId, Guid userId, Guid userContactId)
    {
        var chatUser = await _chatUserRepository.GetByChatIdUserId(chatId, userId);
        var userContact = await _userRepository.GetById(userContactId);
        if (chatUser is not null || userContact is null)
        {
            return null;
        }
        chatUser = new ChatUserEntity
        {
            ChatId = chatId, UserId = userId
        };
        var contact = await _contactRepository.GetByUserIdContactUserId(userId, userContactId);
        if (contact is not null)
        {
            chatUser.Name = contact.ContactName;
            return chatUser;
        }
        chatUser.Name = userContact.UserName ?? userContact.Phone;
        return chatUser;
    }
}