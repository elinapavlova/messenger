﻿using System;

namespace Messenger.Core.Dtos.Contact;

public class ContactCreateResponse
{
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public Guid ContactId { get; set; }
    public string ContactName { get; set; }
    public DateTime DateCreated { get; set; }
}