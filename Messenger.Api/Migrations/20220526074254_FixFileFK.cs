﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Messenger.Api.Migrations
{
    public partial class FixFileFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_Chats_ChatId",
                table: "Files");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Chats_ChatId",
                table: "Files",
                column: "ChatId",
                principalTable: "Chats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_Chats_ChatId",
                table: "Files");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Chats_ChatId",
                table: "Files",
                column: "ChatId",
                principalTable: "Chats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
