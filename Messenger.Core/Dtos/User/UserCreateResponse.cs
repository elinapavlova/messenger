﻿using System;
using Messenger.Common;
using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.User;

public class UserCreateResponse
{
    public Guid Id { get; set; }
    public string UserName { get; set; }
    public string Phone { get; set; }
    public DateTime DateCreated { get; set; }
    public AccessToken AccessToken { get; set; }
}