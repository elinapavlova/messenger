﻿namespace Messenger.Database;

public abstract class BaseEntity<TKey>
{
    public TKey Id { get; set; }
}