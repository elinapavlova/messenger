﻿namespace Messenger.Api.Dtos;

public record ContactUpdateRequestDto
{
    public string NewContactName { get; set; }
}