﻿#nullable enable

namespace Messenger.Common.Filter;

public class FilterDto
{
    public int? PageSize { get; set; }
    public int? PageNumber { get; set; }
}