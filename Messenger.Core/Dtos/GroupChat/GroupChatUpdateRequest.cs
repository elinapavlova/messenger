﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.GroupChat;

public class GroupChatUpdateRequest
{
    public readonly ChatEntity GroupChat;

    public GroupChatUpdateRequest(ChatEntity groupChat)
    {
        GroupChat = groupChat;
    }
}