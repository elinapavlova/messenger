﻿using Messenger.Database.Entities;

namespace Messenger.Core.Dtos.Invitation;

public class ChatEntryByInvitationRequest
{
    public UserEntity User { get; set; }
    public InvitationEntity Invitation { get; set; }
}