﻿using System;
using System.Collections.Generic;

namespace Messenger.Database.Entities;

public class MessageEntity : BaseEntity<Guid>
{
    public string Text { get; set; }
    public Guid ChatId { get; set; }
    public Guid ChatUserId { get; set; }
    public Guid? ReferenceMessageId { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime? DateUpdated{ get; set; }
    public DateTime? DateDeleted { get; set; }
        
    public ChatEntity Chat { get; set; }
    public ChatUserEntity ChatUser { get; set; }
    public List<FileEntity> Files { get; set; }
}