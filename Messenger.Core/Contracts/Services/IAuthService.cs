﻿#nullable enable
using System.Threading.Tasks;
using Messenger.Common;
using Messenger.Core.Dtos.User;

namespace Messenger.Core.Contracts.Services;

public interface IAuthService
{
    Task<UserCreateResponse?> Register(UserCreateRequest request);
    Task<UserLoginResponse?> Login(string phone);
    Task<AccessToken?> RefreshToken(string refreshToken);
}