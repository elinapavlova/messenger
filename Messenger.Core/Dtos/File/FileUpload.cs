﻿using System;

namespace Messenger.Core.Dtos.File;

public class FileUpload
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Path { get; set; }
    public string Extension { get; set; }
    public Uri Url { get; set; }
}