﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messenger.Common.Filter;
using Messenger.Core.Dtos;
using Messenger.Core.Dtos.User;

namespace Messenger.Core.Contracts.Services;

public interface IUserChatService
{
    Task<UserResponse?> LeaveChat(UserLeaveChatRequest request);
    Task<List<UserChatResponse>?> GetChats(Guid userId, FilterDto filter);
}