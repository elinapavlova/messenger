﻿using System.Collections.Generic;

namespace Messenger.Core.Options;

public class StaticFilesOptions
{
    public string BaseUrl { get; set; }
    public Dictionary<string, string> Headers { get; set; }
}